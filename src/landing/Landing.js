import React, { Component } from 'react';
import LoginizationWrapper from '../components/loginizationWrapper/LoginizationWrapper';
import { Link } from 'react-router-dom';
import './Landing.css';

class Landing extends Component {
  render() {
    return (
      <LoginizationWrapper>
        <div className='landing-container'>
          <div className='landing-title'>IT Community</div>
          <p className='landing-description'>
            Create a developer profile/portfolio, share posts and get help from other developers
          </p>
          <div className='buttons-container'>
            <Link className='login-btn' to='/sign-in' replace >Login</Link>
            <Link className='signUp-btn' to='/sign-up' replace >Sign Up</Link>
          </div>
        </div>
      </LoginizationWrapper>
    );
  }
}

export default Landing;