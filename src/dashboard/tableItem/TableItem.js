import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Moment from 'react-moment';
import CustomButton from '../../components/customButton/CustomButton';

class TableItem extends Component {
  handleEditItem =() => {
    const { id, editClick } = this.props;

    editClick(id);
  };

  handleDeleteItem =() => {
    const { id, deleteClick } = this.props;

    deleteClick(id);
  };

  render() {
    const { id, column2, column3, from, to } = this.props;

    return (
      <tr key={ id }>
        <td>{ column2 }</td>
        <td>{ column3 }</td>
        <td>
          <Moment format="YYYY/MM/DD">{ from }</Moment> -
          { to === null ? (
            ' Now'
          ) : (
            <Moment format="YYYY/MM/DD">{ to }</Moment>
          ) }
        </td>
        <td>
          <div className='buttons-container'>
            <CustomButton
              text='Edit'
              clickHandler={ this.handleEditItem }
              className="dashboard-btn"
              fontFamily="Quicksand"
              height={ 25 }
              width={ 120 }
            />
            <CustomButton
              text='Delete'
              clickHandler={ this.handleDeleteItem }
              className="dashboard-btn"
              fontFamily="Quicksand"
              height={ 25 }
              width={ 120 }
            />
          </div>
        </td>
      </tr>
    );
  }
}

TableItem.propTypes = {
  deleteClick: PropTypes.func.isRequired,
  editClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  column2: PropTypes.string.isRequired,
  column3: PropTypes.string.isRequired,
  from: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default TableItem;
