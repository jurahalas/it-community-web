import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as educationActions from '../education/EducationActions';
import { bindActionCreators } from 'redux';
import TableItem from './tableItem/TableItem';

class Education extends Component {
  handleEditEducationClick=(id) => {
    this.props.history.push(`edit-education/${id}`);
  };

  handleDeleteEducationClick=(id) => {
    this.props.educationActions.deleteEducationRequest(id);
  };

  render() {
    const { education } = this.props;

    return (
      <div>
        <h3>Education Credentials</h3>
        <table className="table">
          <thead>
            <tr>
              <th>School</th>
              <th>Degree</th>
              <th>Years</th>
              <th>Controls</th>
            </tr>
            {
              education && education.map((edu, index) => (
                <TableItem
                  key={ index }
                  id={ edu._id }
                  column2={ edu.school }
                  column3={ edu.degree }
                  from={ edu.from }
                  to={ edu.to }
                  editClick={ this.handleEditEducationClick }
                  deleteClick={ this.handleDeleteEducationClick }
                />
              ))
            }
          </thead>
        </table>
      </div>
    );
  }
}

Education.propTypes = {
  educationActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  education: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    educationReducer: state.educationReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    educationActions: bindActionCreators(educationActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Education);
