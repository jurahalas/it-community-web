import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as experienceActions from '../experience/ExperienceActions';
import { bindActionCreators } from 'redux';
import TableItem from './tableItem/TableItem';

class Experience extends Component {
  handleEditExperienceClick=(id) => {
    this.props.history.push(`edit-experience/${id}`);
  };

  handleDeleteExperienceClick=(id) => {
    this.props.experienceActions.deleteExperienceRequest(id);
  };

  render() {
    const { experience } = this.props;

    return (
      <div>
        <h3>Experience Credentials</h3>
        <table className="table">
          <thead>
            <tr>
              <th>Company</th>
              <th>Title</th>
              <th>Years</th>
              <th>Controls</th>
            </tr>
            {
              experience && experience.map((exp, index) => (
                <TableItem
                  key={ index }
                  id={ exp._id }
                  column2={ exp.company }
                  column3={ exp.title }
                  from={ exp.from }
                  to={ exp.to }
                  editClick={ this.handleEditExperienceClick }
                  deleteClick={ this.handleDeleteExperienceClick }
                />
              ))
            }
          </thead>
        </table>
      </div>
    );
  }
}

Experience.propTypes = {
  experienceActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  experience: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    experienceReducer: state.experienceReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    experienceActions: bindActionCreators(experienceActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Experience);
