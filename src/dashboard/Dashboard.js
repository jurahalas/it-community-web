import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Notification from '../components/notification/Notification';
import Spinner from '../components/spinner/SpinnerGrid';
import Experience from './Experience';
import Education from './Education';
import * as profileActions from '../profile/ProfileActions';
import './Dashboard.css';
import CustomButton from '../components/customButton/CustomButton';
import jwt_decode from 'jwt-decode';

class Dashboard extends Component {
  componentDidMount() {
    this.props.profileActions.getCurrentUserProfileRequest();
  }

  handleEditProfileClick =() =>
    this.props.history.push('/edit-profile');

  handleAddExperienceClick =() =>
    this.props.history.push('/create-experience');

  handleAddEducationClick =() => {
    this.props.history.push('/create-education');
  };

  handleCreateProfileClick =() => {
    this.props.history.push('/create-profile');
  };

  render() {
    const { profileData, experienceReducer, educationReducer } = this.props;
    const { loading, userData } = profileData;
    const { name } = jwt_decode(localStorage.access_token);

    return (
      <div className="dashboard-container">
        <Spinner isShow={ experienceReducer.loading || loading || educationReducer.loading } />
        <Notification />
        <h1>Dashboard</h1>
        <h3>
          Welcome <Link to='/my-profile' className="link" >{ userData.name || name }</Link>
        </h3>
        {
          profileData.error.status !== 'NonExist'
            ? (
              <div>
                <CustomButton
                  text='Edit Profile'
                  className='dashboard-btn'
                  color='#ffffff'
                  clickHandler={ this.handleEditProfileClick }
                  height={ 30 }
                />
                <CustomButton
                  text='Add Experience'
                  className='dashboard-btn'
                  color='#ffffff'
                  clickHandler={ this.handleAddExperienceClick }
                  height={ 30 }
                />
                <CustomButton
                  text='Add Education'
                  className='dashboard-btn'
                  color='#ffffff'
                  clickHandler={ this.handleAddEducationClick }
                  height={ 30 }
                />
                <Experience { ...this.props } experience={ userData.experience } />
                <Education { ...this.props } education={ userData.education } />
              </div>
            )
            : (
              <div>
                <h3>You have not yet setup a profile, please add some info</h3>
                <CustomButton
                  text='Create Profile'
                  className='dashboard-btn'
                  color='#ffffff'
                  clickHandler={ this.handleCreateProfileClick }
                  height={ 30 }
                />
              </div>
            )
        }

      </div>
    );
  }
}

Dashboard.propTypes = {
  profileActions: PropTypes.object,
  experienceReducer: PropTypes.object,
  educationReducer: PropTypes.object,
  profileData: PropTypes.object,
  history: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
    experienceReducer: state.experienceReducer,
    educationReducer: state.educationReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);