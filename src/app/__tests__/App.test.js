import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import App from '../App';
import { Provider } from 'react-redux';
import { configureStore } from '../../store/Store';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });
const store = configureStore();

it('renders without crashing', () => {
  shallow(
    <Provider store={ store }>
      <App
        store={ { store } }
        route={ { name: 'test' } }
        history={ { push: jest.fn() } }
      />
    </Provider>);
});