import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
  <div className='container'>
    <div className='row'>
      <div className='col-md-12'>
        <h3>
          The page you are looking for doesn't exists.
          <Link to='/' replace >Home</Link>
        </h3>
      </div>
    </div>
  </div>
);

export default NotFound;
