import React from 'react';
import MainTemplate from '../MainTemplate';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { configureStore } from '../../../store/Store';

Enzyme.configure({ adapter: new Adapter() });
const store = configureStore();

it('renders without crashing', () => {
  shallow(
    <MainTemplate
      history={ { push: jest.fn() } }
      route={ {} }
      store={ store }
    />);
});
