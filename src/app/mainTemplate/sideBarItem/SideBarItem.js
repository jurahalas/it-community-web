import React from 'react';
import PropTypes from 'prop-types';
import './SideBarItem.css';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import { isIEReloadPage } from '../../../services/Helper';

const SideBarItem = ({ url, image, title, isSelected, isSelectedSub, selectTab, sub }) => (
  <div onClick={ selectTab } className={ classnames({ 'sidebar-item': true, 'selected': isSelected, sub, 'selected-sub': isSelectedSub }) }>
    <Link to={ url } className={ classnames({ 'sidebar-item-anchor': true, sub }, { 'side-bar-pointer-event-none': /^\/$/.test(url) }) } replace onClick={ isIEReloadPage }>
      { image && <img alt="logo" src={ image } className="imgContainer" /> }
      { title }
    </Link>
  </div>
);

SideBarItem.defaultProps = {
  url: '/',
  isSelected: false,
};

SideBarItem.propTypes = {
  url: PropTypes.string.isRequired,
  image: PropTypes.string,
  title: PropTypes.string.isRequired,
  isSelected: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  isSelectedSub: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  sub: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  selectTab: PropTypes.func,
};

export default SideBarItem;
