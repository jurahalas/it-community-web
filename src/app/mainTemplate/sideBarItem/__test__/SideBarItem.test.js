import React from 'react';
import SideBarItem from '../SideBarItem';
import Test from './Test.svg';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(<SideBarItem image={ Test } title="Test" isSelected={ false } />);
});
