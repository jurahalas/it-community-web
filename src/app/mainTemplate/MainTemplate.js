import React, { Component } from 'react';
import './MainTemplate.css';
import PropTypes from 'prop-types';
import SideBarItem from './sideBarItem/SideBarItem';
import Collapsible from '../../components/collapsible/Collapsible';
import arrow from '../../images/arrow.svg';
import avatar from '../../images/avatar.svg';
import logout from '../../images/logout.svg';
import Notification from '../../components/notification/Notification';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as signInActions from '../../signIn/SignInActions';
import { Link } from 'react-router-dom';

class MainTemplate extends Component {
  constructor(props) {
    super();

    this.state = {
      selectedTab: props.route.parent,
    };
  }

  static getDerivedStateFromProps(nextProps, state) {
    if(nextProps.route.parent !== state.selectedTab)
      return {
        selectedTab: nextProps.route.parent,
      };
    else
      return null;
  }

  getActiveTab = (props) => (
    props.route.parent || ''
  );

  selectTab = () => {
    this.setState({
      selectedTab: this.getActiveTab(this.props),
    });
  };

  logout = () => {
    const { signInActions, history } = this.props;

    signInActions.logout();
    history.push('/');
  };

  onProfileClick = () => {
    this.props.history.push('/my-profile');
  };

  render() {
    const { selectedTab } = this.state;
    const { profileData } = this.props;
    const { userData: { user } } = profileData;

    return (
      <div className='main-template-container'>
        <Notification />
        { this.props.showSideBar &&
        <div className="template-sideBar-container">
          <div className="template-profile-block">
            <Link to="/" className='template-title'>IT COMMUNITY</Link>
          </div>
          <div>
          </div>
          <SideBarItem
            title="DASHBOARD"
            url="/dashboard"
            selectTab={ this.selectTab }
            isSelected={ selectedTab && selectedTab.indexOf('Dashboard') !== -1 }
          />
          <div className="template-colapsible">
            <Collapsible
              trigger="Posts"
              icon={ arrow }
              isSelectedSub={ selectedTab && (selectedTab.indexOf('Posts') !== -1 || selectedTab.indexOf('MyPosts') !== -1) }
            >
              <SideBarItem
                sub
                title="All Posts"
                url="/posts"
                isSelected={ false }
                isSelectedSub={ selectedTab === 'Posts' }
              />
              <SideBarItem
                sub
                title="My Posts"
                url="/my-posts"
                isSelected={ false }
                isSelectedSub={ selectedTab === 'MyPosts' }
              />
            </Collapsible>
          </div>
          <div className="template-colapsible">
            <Collapsible
              trigger="Profiles"
              icon={ arrow }
              isSelectedSub={ selectedTab && (selectedTab.indexOf('Profiles') !== -1 || selectedTab.indexOf('Profile') !== -1) }
            >
              <SideBarItem
                sub
                title="All Profiles"
                url="/profiles"
                selectTab={ this.selectTab }
                isSelected={ false }
                isSelectedSub={ selectedTab === 'Profiles' }
              />
              <SideBarItem
                sub
                title="My Profile"
                url="/my-profile"
                selectTab={ this.selectTab }
                isSelected={ false }
                isSelectedSub={ selectedTab === 'Profile' }
              />
            </Collapsible>
          </div>
        </div>
        }
        <div className={ this.props.showSideBar ? 'template-header' : 'template-without-sidebar' }>
          { this.props.showSideBar &&
          <div className="template-header-container">
            <div>
            </div>
            <div className="template-header-icons-container">
              <img className='avatar-img' onClick={ this.onProfileClick } alt="avatar" src={ user.avatar || localStorage.avatar || localStorage.googleAvatar || avatar } />
              <img onClick={ this.logout } alt="logout" src={ logout } />
            </div>
          </div>
          }
          <div className='wrapper-container'>
            <div className="template-container">
              { this.props.children }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MainTemplate.propTypes = {
  children: PropTypes.element,
  history: PropTypes.object.isRequired,
  profileData: PropTypes.object.isRequired,
  route: PropTypes.shape({
    parent: PropTypes.string,
  }),
  showSideBar: PropTypes.bool,
  signInActions: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signInActions: bindActionCreators(signInActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MainTemplate));