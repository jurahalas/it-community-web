import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import MainTemplate from './mainTemplate/MainTemplate';

class App extends Component {
  render() {
    const { route, history, showSideBar, store } = this.props;

    return (
      <div>
        <MainTemplate
          route={ route }
          history={ history }
          showSideBar={ showSideBar }
          store={ store }
        >
          { this.props.children }
        </MainTemplate>
      </div>
    );
  }
}

App.defaultProps = {
  showSideBar: true,
};

App.propTypes = {
  children: PropTypes.element,
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  showSideBar: PropTypes.bool,
  store: PropTypes.object.isRequired,
};

export default App;
