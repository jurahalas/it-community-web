import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Store from './store/Store';
import App from './app/App';
import { appHistory } from './services/HistoryConfig';
import { ConnectedRouter } from 'react-router-redux';
import MainContainer from './components/mainContainer/MainContainer';
import NotFound from './app/notFound/NotFound';
import SignIn from './signIn/SignIn';
import SignUp from './signUp/SignUp';
import './styles/index.css';
import Landing from './landing/Landing';
import Dashboard from './dashboard/Dashboard';
import Profile from './profile/Profile';
import EditProfile from './profile/editProfile/EditProfile';
import Profiles from './profile/profiles/Profiles';
import Experience from './experience/Experience';
import Education from './education/Education';
import LoginizationWrapper from './components/loginizationWrapper/LoginizationWrapper';
import Posts from './posts/Posts';
import PostDetails from './posts/postDetails/PostDetails';
import * as postsActions from './posts/PostsActions';
import jwt_decode from 'jwt-decode';

class Main extends Component {
  checkToken = () => (
    localStorage.access_token
  );

  renderStartPage = (props) => (
    this.checkToken(props)
      ? <Redirect to="/dashboard" />
      : <Landing />
  );

  renderSignIn = (props) => (
    !this.checkToken(props)
      ? <SignIn { ...props } route={ { name: 'Login', value: 'Login' } } />
      : <Redirect to="/" />
  );

  renderSignUp = (props) => (
    !this.checkToken(props)
      ? <SignUp { ...props } route={ { name: 'SignUp', value: 'SignUp' } } />
      : <Redirect to="/" />
  );

  renderSuccess = (props) => {
    const token = props.location.search.split('=')[1];

    if(token) {
      const { name, email, avatar, googleAvatar } = jwt_decode(token) || {};

      localStorage.access_token = token;
      localStorage.email = email;
      localStorage.name = name;
      localStorage.avatar = avatar;
      localStorage.googleAvatar = googleAvatar || '';

      return <Redirect to="/dashboard" />;
    }};

  renderDashboard = (props) => (
    this.checkToken(props)
      ? (
        <App { ...props } route={ { name: 'Dashboard', parent: 'Dashboard' } }>
          <Dashboard { ...props } route={ { name: 'Dashboard', parent: 'Dashboard' } } />
        </App>
      )
      : (
        <Redirect to='/sign-in' />
      )
  );

  renderProfile = (props) => (
    this.checkToken(props)
      ? (
        <App { ...props } route={ { name: 'Profile', parent: 'Profile' } }>
          <Profile { ...props } route={ { name: 'Profile', parent: 'Profile' } } />
        </App>
      )
      : (
        <Redirect to='/sign-in' />
      )
  );

  renderEditProfile = (props) => (
    this.checkToken(props)
      ? (
        <App { ...props } route={ { name: 'Profile', parent: 'Profile' } }>
          <EditProfile { ...props } route={ { name: 'EditProfile', parent: 'EditProfile' } } />
        </App>
      )
      : (
        <Redirect to='/sign-in' />
      )
  );

  renderProfiles = (props) => (
    this.checkToken(props)
      ? (
        <App { ...props } route={ { name: 'Profiles', parent: 'Profiles' } }>
          <Profiles { ...props } route={ { name: 'Profiles', parent: 'Profiles' } } />
        </App>
      )
      : (
        <LoginizationWrapper>
          <Profiles { ...props } route={ { name: 'Profiles', parent: 'Profiles' } } />
        </LoginizationWrapper>
      )
  );

  renderUserProfile = (props) => {
    const { handle } = props.match.params;

    return (
      this.checkToken(props)
        ? (
          <App { ...props } route={ { name: 'Profiles', parent: 'Profiles' } }>
            <Profile { ...props } route={ { name: 'UserProfile', parent: 'UserProfile', handle } } />
          </App>
        )
        : (
          <LoginizationWrapper publicView>
            <Profile { ...props } route={ { name: 'UserProfile', parent: 'UserProfile', handle } } />
          </LoginizationWrapper>
        )
    );
  };

  renderEditExperience = (props) => {
    const { id } = props.match.params;

    return (
      this.checkToken(props)
        ? (
          <App { ...props } route={ { name: 'Experience', parent: 'Experience' } }>
            <Experience { ...props } route={ { name: 'Experience', parent: 'Experience', id } } />
          </App>
        )
        : (
          <Redirect to='/sign-in' />
        )
    );
  };

  renderEditEducation = (props) => {
    const { id } = props.match.params;

    return (
      this.checkToken(props)
        ? (
          <App { ...props } route={ { name: 'Education', parent: 'Education' } }>
            <Education { ...props } route={ { name: 'Education', parent: 'Education', id } } />
          </App>
        )
        : (
          <Redirect to='/sign-in' />
        )
    );
  };

  renderPosts = (props) => {
    if(this.checkToken(props)) {
      Store.dispatch(postsActions.getPostsRequest());

      return (
        <App { ...props } route={ { name: 'Posts', parent: 'Posts' } }>
          <Posts { ...props } route={ { name: 'Posts', parent: 'Posts' } } />
        </App>
      );
    } else
      return <Redirect to='/sign-in' />;
  };

  renderMyPosts = (props) => {
    if(this.checkToken(props)) {
      Store.dispatch(postsActions.getCurrentUserPostsRequest(props.match.params.id));

      return (
        <App { ...props } route={ { name: 'MyPosts', parent: 'MyPosts' } }>
          <Posts { ...props } route={ { name: 'MyPosts', parent: 'MyPosts' } } />
        </App>
      );
    } else
      return <Redirect to='/sign-in' />;
  };

  renderPostDetails = (props) => {
    const { id } = props.match.params;

    return (
      this.checkToken(props)
        ? (
          <App { ...props } route={ { name: 'PostDetails', parent: 'PostDetails' } }>
            <PostDetails { ...props } route={ { name: 'PostDetails', parent: 'PostDetails', id } } />
          </App>
        )
        : (
          <Redirect to='/sign-in' />
        )
    );
  };

  render() {
    return (
      <Provider store={ Store }>
        <ConnectedRouter history={ appHistory }>
          <MainContainer>
            <Router>
              <Switch>
                <Route
                  exact
                  path="/"
                  render={ this.renderStartPage }
                />
                <Route
                  exact
                  name='SignIn'
                  path="/sign-in"
                  render={ this.renderSignIn }
                />
                <Route
                  exact
                  name='SignUp'
                  path="/sign-up"
                  render={ this.renderSignUp }
                />
                <Route
                  exact
                  name='Home'
                  path="/success"
                  render={ this.renderSuccess }
                />
                <Route
                  exact
                  name='Dashboard'
                  path="/dashboard"
                  render={ this.renderDashboard }
                />
                <Route
                  exact
                  name='Profile'
                  path="/my-profile"
                  render={ this.renderProfile }
                />
                <Route
                  exact
                  name='Profile'
                  path="/create-profile"
                  render={ this.renderEditProfile }
                />
                <Route
                  exact
                  name='Profile'
                  path="/edit-profile"
                  render={ this.renderEditProfile }
                />
                <Route
                  exact
                  name='Profiles'
                  path="/profiles"
                  render={ this.renderProfiles }
                />
                <Route
                  exact
                  name='Profiles'
                  path="/profile/:handle"
                  render={ this.renderUserProfile }
                />
                <Route
                  exact
                  name='Experience'
                  path="/create-experience"
                  render={ this.renderEditExperience }
                />
                <Route
                  exact
                  name='Experience'
                  path="/edit-experience/:id"
                  render={ this.renderEditExperience }
                />
                <Route
                  exact
                  name='Education'
                  path="/create-education"
                  render={ this.renderEditEducation }
                />
                <Route
                  exact
                  name='Education'
                  path="/edit-education/:id"
                  render={ this.renderEditEducation }
                />
                <Route
                  exact
                  name='Posts'
                  path="/posts"
                  render={ this.renderPosts }
                />
                <Route
                  exact
                  name='Posts'
                  path="/my-posts"
                  render={ this.renderMyPosts }
                />
                <Route
                  exact
                  name='PostDetails'
                  path="/post/:id"
                  render={ this.renderPostDetails }
                />
                <Route name="NotFound" component={ NotFound } />
              </Switch>
            </Router>
          </MainContainer>
        </ConnectedRouter>
      </Provider>
    );
  }
}

ReactDOM.render(
  <Main />,
  document.getElementById('root')
);