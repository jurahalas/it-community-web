import React from 'react';
import AvatarUpload from '../AvatarUpload';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(
    <AvatarUpload
      defaultZoom={ 1 }
      stepZoom={ 0.01 }
      minZoom={ 0.5 }
      maxZoom={ 2 }
      open={ false }
      loading={ false }
      onCancel={ jest.fn() }
      onSubmit={ jest.fn() }
    />
    , document.createElement('div'));
});
