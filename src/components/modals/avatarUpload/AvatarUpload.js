import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import BaseModalWindow from '../../baseModalWindow/BaseModalWindow';
import { ACCEPTED_IMAGES_MIME_TYPES } from '../../../services/Constants';
import { filterFilesByMimeTypes } from '../../../services/Files';
import bowser from 'bowser';
import './AvatarUpload.css';
import ReactAvatarEditor from 'react-avatar-editor';

export default class AvatarUpload extends Component {
  static get isDNDSupported() {
    return !bowser.msie && !bowser.msedge;
  }

  constructor({ defaultZoom }) {
    super();

    this.state = {
      file: '',
      name: '',
      croppedImage: '',
      dragOver: false,
      dndNotSupported: false,
      invalidFilesType: false,
      zoom: defaultZoom,
    };
  }

  componentDidUpdate(prevProps) {
    if(prevProps.open !== this.props.open)
      this.handleReset();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  handlePreviewDragOver = (e) => {
    if(!AvatarUpload.isDNDSupported)
      return this.setState({
        dragOver: true,
        dndNotSupported: true,
      });

    const { items } = e.dataTransfer;
    const invalidFilesType = Object.keys(items).some(key => {
      const item = items[key];

      return ACCEPTED_IMAGES_MIME_TYPES.indexOf(item.type) === -1;
    });

    this.setState({
      dragOver: true,
      invalidFilesType,
    });
  };

  handlePreviewDragLeave = () => {
    this.setState({
      dragOver: false,
      dndNotSupported: false,
      invalidFilesType: false,
    });
  };

  handleFileChange = (e) => {
    const { files } = e.target;
    const filteredFiles = filterFilesByMimeTypes(files, ACCEPTED_IMAGES_MIME_TYPES);
    const file = filteredFiles[0];

    if(!file)
      return this.setState({
        invalidFilesType: true,
      });

    const fileReader = new FileReader();

    fileReader.onload = e => {
      if(!this.unmounted)
        this.setState({
          file: e.target.result,
          name: Date.now(),
          invalidFilesType: false,
        });
    };

    fileReader.readAsDataURL(file);
    e.target.value = '';
  };

  handleReset = () => {
    const { defaultZoom } = this.props;

    this.setState({
      file: '',
      name: '',
      croppedImage: '',
      dragOver: false,
      dndNotSupported: false,
      invalidFilesType: false,
      zoom: defaultZoom,
    });
  };

  handleAvatarChangeEnd = () => {
    this.setState({
      croppedImage: this.refEditor.getImageScaledToCanvas(),
    });
  };

  handleChangeZoom = (e) => {
    this.setState({
      zoom: Number(e.target.value),
    });
  };

  handleSubmit = () => {
    const { onSubmit } = this.props;
    const { croppedImage, name } = this.state;

    onSubmit(croppedImage, name);
  };

  setRefEditor = (node) => this.refEditor = node;

  render() {
    const { minZoom, maxZoom, stepZoom, open, loading, onCancel } = this.props;
    const { file, zoom, dragOver, dndNotSupported, invalidFilesType, croppedImage } = this.state;

    return (
      <BaseModalWindow
        className='avatar-upload-modal'
        width={ 600 }
        height={ 500 }
        isOpen={ open }
        loading={ loading }
        inline
        isShowBackground
        disabledSubmit={ !croppedImage }
        cancelClickHandler={ onCancel }
        submitClickHandler={ this.handleSubmit }
      >
        <div className='avatar-upload-modal-title'>
          Upload an Avatar
        </div>
        <div
          className={ classNames('avatar-upload-modal-preview', {
            'm-dragover': dragOver,
            'm-dragover-invalid': invalidFilesType || dndNotSupported,
            'm-with-file': file,
          }) }
          onDragOver={ this.handlePreviewDragOver }
          onDragLeave={ this.handlePreviewDragLeave }
        >
          {
            file
              ? (
                <ReactAvatarEditor
                  ref={ this.setRefEditor }
                  image={ file }
                  width={ 250 }
                  height={ 250 }
                  border={ 0 }
                  color={ [255, 255, 255, 0.6] }
                  scale={ zoom }
                  rotate={ 0 }
                  onImageChange={ this.handleAvatarChangeEnd }
                  onLoadSuccess={ this.handleAvatarChangeEnd }
                />
              )
              : (
                <Fragment>
                  <div className='avatar-upload-modal-preview-text'>
                    {
                      `Press ${!AvatarUpload.isDNDSupported ? '' : 'or Drag and Drop'} here`
                    }
                  </div>
                  <input
                    className='avatar-upload-modal-preview-input'
                    type='file'
                    accept={ ACCEPTED_IMAGES_MIME_TYPES.join(', ') }
                    onChange={ this.handleFileChange }
                  />
                </Fragment>
              )
          }
        </div>
        { invalidFilesType &&
        <div className='avatar-upload-modal-controls m-error'>
          You can upload only PNG or JPG images.
        </div>
        }
        { dndNotSupported &&
        <div className='avatar-upload-modal-controls m-error'>
          Drag'n'Drop not supported by your browser.
        </div>
        }
        { file &&
        <div className='avatar-upload-modal-controls'>
          <input
            className='avatar-upload-modal-controls-zoom'
            type='range'
            min={ minZoom }
            max={ maxZoom }
            step={ stepZoom }
            value={ zoom }
            onChange={ this.handleChangeZoom }
          />
          <div
            className='avatar-upload-modal-controls-clear'
            onClick={ this.handleReset }
          >
            Reset
          </div>
        </div>
        }
      </BaseModalWindow>
    );
  }
}

AvatarUpload.propTypes = {
  defaultZoom: PropTypes.number,
  stepZoom: PropTypes.number,
  minZoom: PropTypes.number,
  maxZoom: PropTypes.number,
  open: PropTypes.bool,
  loading: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
};

AvatarUpload.defaultProps = {
  defaultZoom: 1,
  stepZoom: 0.01,
  minZoom: 0.5,
  maxZoom: 2,
  open: false,
  loading: false,
  onSubmit() {
  },
};
