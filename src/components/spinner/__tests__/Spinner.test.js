import React from 'react';
import ReactDOM from 'react-dom';
import SpinnerGrid from '../SpinnerGrid';
import renderer from "react-test-renderer";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(<SpinnerGrid isShow={ false } />, div);
});

test('has a valid snapshot', () => {
  const component = renderer.create(
    <SpinnerGrid
      isShow={ false }
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
