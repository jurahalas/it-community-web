import React from 'react';
import PropTypes from 'prop-types';
import './SpinnerGrid.css';
import classNames from 'classnames';

const SpinnerGrid = (props) => {
  const { isShow } = props;

  return (
    <div className={ classNames('spinner-grid-display-default', { 'spinner-grid-display': isShow }) }>
      <div className="lds-css ng-scope">
        <div className="lds-spin-grid" style={ { height: 'height:100%' } }>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
          <div>
            <div />
          </div>
        </div>
      </div>
    </div>
  );
};

SpinnerGrid.propTypes = {
  isShow: PropTypes.bool.isRequired,
};

export default SpinnerGrid;
