import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputComponent from '../../inputComponent/InputComponent';
import './SignUp.css';
import { Field, reduxForm } from 'redux-form';
import CustomButton from '../../customButton/CustomButton';
import { Link } from 'react-router-dom';
import email from '../../../images/email.svg';
import password from '../../../images/password.svg';
import name from '../../../images/name.svg';
import { signUpValidate } from '../../../services/FormValidate';
import classNames from 'classnames';
import GenericSpinner from '../../spinner/GenericSpinner';

class SignUp extends Component {
  constructor() {
    super();

    this.state = {
      prevDate: '',
      emailError: '',
    };
  }

  componentDidMount() {
    this.props.initialize(this.props.signUpReducer.formData);
  }

  static getDerivedStateFromProps(nextProps, state) {
    if(nextProps.signUpReducer.errors !== state.emailError)
      return {
        emailError: nextProps.signUpReducer.errors,
      };
    else
      return null;
  }

  onSubmit = (formData) => {
    this.props.onSubmit(formData);
  };

  renderField = ({ icon, label, meta: { error, touched, invalid }, input: { onChange, value, onBlur, onFocus, name }, emailError }) => {
    const checkType = name === 'password' || name === 'confirm_password';

    return (
      <div className={ classNames('form-group', { 'has-error ': (invalid && error && touched) || emailError }) }>
        <InputComponent
          iconSrc={ icon }
          placeholder={ label }
          onChange={ onChange }
          value={ value }
          onBlur={ onBlur }
          onFocus={ onFocus }
          error={ (error && touched) || (emailError && name === 'email') }
          type={ checkType ? 'password' : 'text' }
        />
        { (invalid && error && touched) || (emailError && name === 'email')
          ? (
            <div className='error-message-container'>
              <div className="error-message">{ emailError || error }</div>
            </div>
          )
          : (
            <div className="helpBlock" />
          )
        }
      </div>
    );
  };

  render() {
    const { handleSubmit, signUpReducer } = this.props;

    return (
      <div className='sign-up-form-wrapper'>
        <div className="sign-up-title">Sign Up</div>
        <form className="signUpForm" onSubmit={ handleSubmit(this.onSubmit) }>
          <Field
            icon={ name }
            name="name"
            type="text"
            component={ this.renderField }
            label="Name"
          />
          <Field
            icon={ email }
            name="email"
            type="email"
            component={ this.renderField }
            label="Enter Email"
            emailError={ this.state.emailError }
          />
          <Field
            icon={ password }
            name="password"
            type="text"
            component={ this.renderField }
            label="Enter Password"
          />
          <Field
            icon={ password }
            name="confirm_password"
            type="text"
            component={ this.renderField }
            label="Confirm Password"
          />
        </form>
        <CustomButton
          text={ signUpReducer.loading ? '' : 'Sign Up' }
          clickHandler={ handleSubmit(this.onSubmit) }
          className="submit"
          fontFamily="Quicksand"
          height={ 40 }
          width={ 375 }
        />
        { signUpReducer.loading &&
        <GenericSpinner
          borderWidth={ 4 }
          height={ 27 }
          width={ 27 }
          borderColor="rgb(255, 255, 255)"
        />
        }
        <div className='sign-up-link-container'>
          Already have an account? <Link to='/sign-in'>Sign In</Link>
        </div>
      </div>
    );
  }
}

SignUp.propTypes = {
  signUpReducer: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  initialize: PropTypes.func,
};

SignUp = reduxForm({
  form: 'SignUp',
  validate: signUpValidate,
})(SignUp);

export default SignUp;
