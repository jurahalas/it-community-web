import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextArea from '../../textArea/TextArea';
import './PostForm.css';
import CustomButton from '../../customButton/CustomButton';
import { createPostValidate } from '../../../services/FormValidate';
import classNames from 'classnames';
import { Field, reduxForm } from 'redux-form';

class PostForm extends Component {
  onSubmit = (formData) => {
    this.props.onSubmit(formData);
    this.props.initialize(true);
  };

  renderField = ({ placeholder, type, meta: { error, touched, invalid }, input: { onChange, value, name, onBlur, onFocus } }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
      <TextArea
        placeholder={ placeholder }
        type={ type }
        onChange={ onChange }
        value={ value }
        onBlur={ onBlur }
        onFocus={ onFocus }
        error={ error && touched }
        onKeyDown={ this.handleKeyDown }
      />
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="post-form-container">
        <div className="card-header">Say Somthing...</div>
        <div className="card-body-container">
          <form onSubmit={ handleSubmit(this.onSubmit) }>
            <Field
              name="post"
              type="text"
              component={ this.renderField }
              placeholder="Create a post"
            />
            <CustomButton
              text='Submit'
              clickHandler={ handleSubmit(this.onSubmit) }
              className="submit-post"
              height={ 30 }
            />
          </form>
        </div>
      </div>
    );
  }
}

PostForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
};

PostForm = reduxForm({
  form: 'Post',
  validate: createPostValidate,
})(PostForm);

export default PostForm;
