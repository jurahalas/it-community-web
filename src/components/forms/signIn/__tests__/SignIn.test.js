import React from 'react';
import SignIn from '../SignIn';
import { Provider } from 'react-redux';
import Enzyme, { shallow } from 'enzyme';
import Store from '../../../../store/Store';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <SignIn
        SignInActions={ { formSubmit: jest.fn() } }
        handleSubmit={ jest.fn() }
        invalid
      />
    </Provider>
    , div);
});
