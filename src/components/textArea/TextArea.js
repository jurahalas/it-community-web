import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

const TextAreaFieldGroup = ({
  type,
  placeholder,
  value,
  error,
  info,
  onChange,
}) => (
  <div className="form-group">
    <textarea
      className={ classnames('form-control-lg', {
        'is-invalid': error,
      }) }
      placeholder={ placeholder }
      name={ type }
      value={ value }
      onChange={ onChange }
    />
    { info && <small className="text-muted">{ info }</small> }
    { error && <div className="invalid-feedback">{ error }</div> }
  </div>
);

TextAreaFieldGroup.propTypes = {
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  info: PropTypes.string,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default TextAreaFieldGroup;
