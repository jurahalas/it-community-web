import { notificationActionTypes } from './NotificationConstants';

export function createNotification(message, error = false, warning = false) {
  return {
    type: notificationActionTypes.CREATE_NOTIFICATION_MESSAGE,
    payload: {
      message,
      error,
      warning,
    },
  };
}
