import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Notification from '../Notification';
import Store from '../../../store/Store';
import renderer from "react-test-renderer";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(<Provider store={ Store }><Notification notification={ {} } /></Provider>, div);
});

test('has a valid snapshot', () => {
  const component = renderer.create(
    <Provider store={ Store }>
      <Notification notification={ {} } />
    </Provider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});