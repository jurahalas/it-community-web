import { notificationActionTypes } from './NotificationConstants';

const initialState = {
  message: '',
  error: false,
  warning: false,
};

export default function notificationReducer(state = initialState, action) {
  switch(action.type) {

    case notificationActionTypes.CREATE_NOTIFICATION_MESSAGE:
      return {
        ...state,
        message: action.payload.message,
        error: action.payload.error,
        warning: action.payload.warning,
      };

    default:
      return state;

  }
}
