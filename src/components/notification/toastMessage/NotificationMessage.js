import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './NotificationMessage.css';
import classNames from 'classnames';
import closeIcon from '../../../images/close.svg';
import iconSuccess from '../../../../src/images/green.svg';
import iconError from '../../../../src/images/rad.svg';
import iconWarning from '../../../images/yellow.svg';

class NotificationMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  }

  componentDidMount() {
    this.setState({
      visible: true,
    });
    const timeout = 4000;

    setTimeout(this.closeMessage, timeout);
  }

  closeMessage = () => {
    this.setState({
      visible: false,
    });
    this.props.clearMessages();
  };

  render() {
    const toastClassName = classNames(
      'notification-message-container',
      {
        'notification-hide': !this.state.visible,
        'notification-show': this.state.visible,
        'notification-success': !this.props.error,
        'notification-error': this.props.error,
        'notification-warning': !this.props.error && this.props.warning,

      }
    );
    const notificationIconClassName = classNames(
      'notification-icon',
      {
        'success-icon': !this.props.error,
        'error-icon': this.props.error,
        'warning-icon': !this.props.error && this.props.warning,
      }
    );

    return (
      <div className={ toastClassName } >
        <div className={ notificationIconClassName }>
          <img alt="notification-icon" src={ this.props.error ? iconError : !this.props.error && this.props.warning ? iconWarning : iconSuccess } />
        </div>
        <div className="message-text" >
          { this.props.message }
        </div>
        <div className="notification-close-container">
          <img alt="close-notification" src={ closeIcon } onClick={ this.closeMessage } />
        </div>
      </div>
    );
  }
}
NotificationMessage.propTypes = {
  message: PropTypes.string,
  error: PropTypes.bool,
  warning: PropTypes.bool,
  clearMessages: PropTypes.func,
};

export default NotificationMessage;
