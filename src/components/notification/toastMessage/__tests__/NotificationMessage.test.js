import React from 'react';
import ReactDOM from 'react-dom';
import NotificationMessage from '../NotificationMessage';
import renderer from "react-test-renderer";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <NotificationMessage
      message="test"
      error={ false }
    />, div);
});

test('has a valid snapshot', () => {
  const component = renderer.create(
    <NotificationMessage
      message="test"
      error={ false }
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
