import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './LoginizationWrapper.css';
import Notification from '../../components/notification/Notification';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

class LoginizationWrapper extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      token: '',
      resetModalOpen: false,
    };
  }

  render() {
    const { children, publicView } = this.props;

    return (
      <div className={ classNames('loginization-wrapper-container', { 'public-view': publicView }) }>
        <div className="loginization-header">
          <div className='left-block'>
            <Link className='link' to='/'>IT Community</Link>
            <Link className='link' to='/profiles'>Profiles</Link>
          </div>
          <div className='right-block'>
            <Link className='link' to='/sign-up'>Sign Up</Link>
            <Link className='link' to='/sign-in'>Login</Link>
          </div>
        </div>
        <div className="loginization-container">
          <Notification />
          <div className='loginization-content'>
            { children }
          </div>

        </div >
        <div className="loginization-footer">&copy;{ (new Date().getFullYear()) } IT COMMUNITY</div>
      </div>
    );
  }
}
LoginizationWrapper.defaultProps = {
  publicView: false,
};

LoginizationWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
  ]),
  publicView: PropTypes.bool,
};

export default LoginizationWrapper;