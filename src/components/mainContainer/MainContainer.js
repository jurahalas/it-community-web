import React, { Component } from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { withRouter } from 'react-router';
import _includes from 'lodash/includes';

class MainContainer extends Component {
  componentDidMount () {
    this.resetPassword(this.props);
  }

  resetPassword = (props) => {
    if(window.location.hash && _includes(window.location.hash, 'resetToken')) {
      const hashLink = window.location.hash.split('?')[1].split('/');
      const parsed = queryString.parse(hashLink[0]);

      if(parsed && parsed.resetToken && parsed.resetToken.length !== 0)
        localStorage.reset_token = parsed.resetToken;
      props.history.replace(`/${hashLink[1]}`);
    }
  };

  render() {
    return (
      <div>
        { this.props.children }
      </div>
    );
  }
}

MainContainer.propTypes = {
  children: PropTypes.object,
};

export default withRouter(MainContainer);
