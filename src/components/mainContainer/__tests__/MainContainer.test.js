import React from 'react';
import MainContainer from '../MainContainer';
import { Provider } from 'react-redux';
import Store from '../../../store/Store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <MainContainer
        location={ {} }
        history={ {} }
      />
    </Provider>, div);
});
