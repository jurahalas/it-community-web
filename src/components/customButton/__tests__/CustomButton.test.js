import React from 'react';
import ReactDOM from 'react-dom';
import ButtonCustom from '../CustomButton';
import renderer from "react-test-renderer";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(<ButtonCustom />, div);
});

test('has a valid snapshot', () => {
  const component = renderer.create(
    <ButtonCustom />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
