import React from 'react';
import ReactDOM from 'react-dom';
import InputComponent from '../InputComponent';
import renderer from "react-test-renderer";
import Store from "../../../store/Store";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <InputComponent
      placeholder="Search"
    />, div);
});

test('has a valid snapshot', () => {
  const component = renderer.create(
    <InputComponent
      placeholder="Search"
    />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});