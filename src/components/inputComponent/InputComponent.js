import React from 'react';
import PropTypes from 'prop-types';
import './InputComponent.css';
import errorIcon from '../../images/errorstext.svg';
import InputElement from 'react-input-mask';

const InputComponent = (props) => {
  const {
    classWrapperDiv,
    className,
    type,
    placeholder,
    disabled,
    onFocus,
    onBlur,
    onKeyDown,
    width,
    value,
    onChange,
    name,
    iconSrc,
    error,
    mask,
    onClick,
  } = props;

  return (
    <div className={ classWrapperDiv }>
      { iconSrc && <img src={ iconSrc } className="icon-for-input" alt="Input icon" /> }
      <InputElement
        className={ className }
        type={ type }
        placeholder={ placeholder }
        disabled={ disabled }
        onFocus={ onFocus }
        onBlur={ onBlur }
        onKeyDown={ onKeyDown }
        onChange={ onChange }
        onClick={ onClick }
        style={ { width } }
        value={ value }
        name={ name }
        mask={ mask }
        maxLength='256'
      />
      { error && <img src={ errorIcon } className="error-for-input" alt="Input error icon" /> }
    </div>
  );
};

InputComponent.defaultProps = {
  classWrapperDiv: 'wrap-input-field',
  className: 'input-field',
  type: 'text',
  placeholder: 'Input',
  disabled: false,
  width: '',
  name: '',
  iconSrc: '',
};

InputComponent.propTypes = {
  classWrapperDiv: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onKeyDown: PropTypes.func,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  value: PropTypes.string,
  error: PropTypes.any,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  name: PropTypes.string,
  iconSrc: PropTypes.string,
  mask: PropTypes.string,
};

export default InputComponent;
