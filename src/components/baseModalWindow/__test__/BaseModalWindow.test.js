import React from 'react';
import ReactDOM from 'react-dom';
import BaseModalWindow from '../BaseModalWindow';
import renderer from "react-test-renderer";

it('renders without crashing', () => {
  const div = document.createElement('div');

  ReactDOM.render(
    <BaseModalWindow
      isOpen
      children='Content'
    />, div);
});
