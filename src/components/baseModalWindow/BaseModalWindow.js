import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import './BaseModalWindow.css';
import CustomButton from '../customButton/CustomButton';
import classNames from 'classnames';
import Spinner from '../spinner/SpinnerGrid';
import bowser from 'bowser';

// TODO: This component should be fully refactored after all priority task will be completed
// TODO: Make modal closable via Escape keypress

class BaseModalWindow extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    if(nextProps.isOpen === true)
      document.body.classList.add('hide-scroll');

    else if(nextProps.isOpen === false)
      document.body.classList.remove('hide-scroll');

    else return null;
  }

  componentWillUnmount() {
    if(document.body.classList.contains('hide-scroll'))
      document.body.classList.remove('hide-scroll');
  }

  handleClickOverlay = (e) => {
    const { disableBackdropClick, cancelClickHandler } = this.props;

    if(!disableBackdropClick && e.target === this.refOverlay && cancelClickHandler)
      cancelClickHandler(e);
  };

  get isContentRendering() {
    const { isOpen, loading, isContentRenderingDuringLoading } = this.props;

    if(!isOpen)
      return false;

    return isContentRenderingDuringLoading ? true : !loading;
  }

  render() {
    const {
      children,
      isOpen,
      iconForBaseModal,
      isShowBackground,
      height,
      customPosition,
      width,
      className,
      classNameContent,
      classNameButtons,
      textSubmit,
      cancelClickHandler,
      submitClickHandler,
      textCancel,
      isShowCancel,
      isShowSubmit,
      spinnerBtn,
      spinnerSubmitForm,
      loading,
      disabledSubmit,
      buttonCancelProps = {},
      buttonSubmitProps = {},
    } = this.props;

    // TODO: Temporary fallback to have consistent make sure that visually it's same as it was before
    // TODO: Remove it during refactoring of this component(prefer plain styles to inline-sytyles)
    const style = {
      maxWidth: width,
      minHeight: height,
    };
    const classNamesMain = classNames('base-modal-main', {
      [className]: className,
      'base-modal-pointer-none': spinnerSubmitForm,
      msie: bowser.msie,
    });

    return (
      (isOpen || loading) &&
      ReactDOM.createPortal(
        <div
          className={ classNames('base-modal-overlay', { 'm-without-bg': !isShowBackground }) }
          ref={ node => this.refOverlay = node }
          onClick={ this.handleClickOverlay }
        >
          <div style={ style } className={ customPosition ? `${classNamesMain} custom-position` : classNamesMain }>
            <div className='base-modal-main-wrapper'>
              { iconForBaseModal && iconForBaseModal !== '' &&
              <div className="base-modal-icon">
                <img src={ iconForBaseModal } alt="Icon" />
              </div>
              }
              <div className={ classNames('base-modal-content', classNameContent) }>
                { this.isContentRendering && children }
                <Spinner isShow={ loading } />
              </div>
              <div className='base-modal-bottom'>
                <div className={ classNames('base-modal-bottom-buttons', classNameButtons) }>
                  { isShowCancel && !loading &&
                  <CustomButton
                    { ...buttonCancelProps }
                    className={ classNames('base-modal-bottom-cancel', buttonCancelProps.className) }
                    text={ textCancel }
                    color='#005777'
                    clickHandler={ !spinnerSubmitForm && cancelClickHandler }
                    tabIndex='0'
                    onKeyDown={ !spinnerSubmitForm && cancelClickHandler }
                  />
                  }
                  { isShowSubmit && !loading &&
                  <CustomButton
                    { ...buttonCancelProps }
                    className={ classNames('base-modal-bottom-submit', buttonSubmitProps.className) }
                    text={ textSubmit }
                    spinnerBtn={ spinnerBtn }
                    disabled={ spinnerSubmitForm || disabledSubmit }
                    clickHandler={ submitClickHandler }
                    tabIndex='0'
                  />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>,
        document.querySelector('body')
      )
    );
  }
}

BaseModalWindow.defaultProps = {
  children: 'Content',
  isOpen: false,
  height: 300,
  width: 300,
  className: '',
  classNameContent: '',
  classNameButtons: '',
  textCancel: 'Cancel',
  textSubmit: 'Submit',
  isShowCancel: true,
  isShowSubmit: true,
  classNameCancelButton: '',
  classNameSubmitButton: '',
  spinnerBtn: false,
  spinnerSubmitForm: false,
  customPosition: false,
  iconForBaseModal: '',
  isShowBackground: false,
  loading: false,
  isContentRenderingDuringLoading: false,
  disabledSubmit: false,
  disableBackdropClick: true,
  buttonCancelProps: {},
  buttonSubmitProps: {},
  cancelClickHandler() {},
};

BaseModalWindow.propTypes = {
  disabledSubmit: PropTypes.bool,
  disableBackdropClick: PropTypes.bool,
  actionOnOpen: PropTypes.func,
  isOpen: PropTypes.bool,
  loading: PropTypes.bool,
  isContentRenderingDuringLoading: PropTypes.bool,
  isShowCancel: PropTypes.bool,
  isShowSubmit: PropTypes.bool,
  height: PropTypes.any,
  width: PropTypes.any,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  classNameContent: PropTypes.string,
  classNameButtons: PropTypes.string,
  textCancel: PropTypes.string,
  textSubmit: PropTypes.string,
  cancelClickHandler: PropTypes.func,
  submitClickHandler: PropTypes.func,
  spinnerBtn: PropTypes.bool,
  spinnerSubmitForm: PropTypes.bool,
  customPosition: PropTypes.bool,
  iconForBaseModal: PropTypes.string,
  isShowBackground: PropTypes.bool,
  buttonCancelProps: PropTypes.object,
  buttonSubmitProps: PropTypes.object,
};

export default BaseModalWindow;
