import { experienceActionTypes } from './ExperienceConstants';

export function getExperienceRequest(id) {
  return {
    type: experienceActionTypes.GET_EXPERIENCE_REQUEST,
    payload: {
      id,
    },
  };
}

export function getExperienceSuccess(data) {
  return {
    type: experienceActionTypes.GET_EXPERIENCE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getExperienceError(errors) {
  return {
    type: experienceActionTypes.GET_EXPERIENCE_ERROR,
    payload: {
      errors,
    },
  };
}

export function createExperienceRequest(formData) {
  return {
    type: experienceActionTypes.CREATE_EXPERIENCE_REQUEST,
    payload: {
      formData,
    },
  };
}

export function createExperienceSuccess(data) {
  return {
    type: experienceActionTypes.CREATE_EXPERIENCE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function createExperienceError(errors) {
  return {
    type: experienceActionTypes.CREATE_EXPERIENCE_ERROR,
    payload: {
      errors,
    },
  };
}

export function updateExperienceRequest(formData, id) {
  return {
    type: experienceActionTypes.UPDATE_EXPERIENCE_REQUEST,
    payload: {
      formData,
      id,
    },
  };
}

export function updateExperienceSuccess(data) {
  return {
    type: experienceActionTypes.UPDATE_EXPERIENCE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function updateExperienceError(errors) {
  return {
    type: experienceActionTypes.UPDATE_EXPERIENCE_ERROR,
    payload: {
      errors,
    },
  };
}

export function deleteExperienceRequest(id) {
  return {
    type: experienceActionTypes.DELETE_EXPERIENCE_REQUEST,
    payload: {
      id,
    },
  };
}

export function deleteExperienceSuccess() {
  return {
    type: experienceActionTypes.DELETE_EXPERIENCE_SUCCESS,
  };
}

export function deleteExperienceError(errors) {
  return {
    type: experienceActionTypes.DELETE_EXPERIENCE_ERROR,
    payload: {
      errors,
    },
  };
}