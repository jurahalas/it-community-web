import { processRequest } from '../services/Api';
import { all, takeLatest } from 'redux-saga/effects';
import { put, call } from 'redux-saga/effects';
import { replace } from 'react-router-redux';
import { experienceActionTypes } from './ExperienceConstants';
import * as notificationActions from '../components/notification/NotificationActions';
import * as experienceActions from './ExperienceActions';
import * as profileActions from '../profile/ProfileActions';

export default function* () {
  yield all([
    yield takeLatest(experienceActionTypes.CREATE_EXPERIENCE_REQUEST, handleCreateExperience),
    yield takeLatest(experienceActionTypes.GET_EXPERIENCE_REQUEST, handleGetExperience),
    yield takeLatest(experienceActionTypes.UPDATE_EXPERIENCE_REQUEST, handleUpdateExperience),
    yield takeLatest(experienceActionTypes.DELETE_EXPERIENCE_REQUEST, handleDeleteExperience),
  ]);
}

export function* handleCreateExperience(action) {
  try {
    const { title, company, location, from, to, current, description } = action.payload.formData;

    const requestPayload = {
      title,
      company,
      location,
      from,
      to: current ? '' : to,
      current,
      description,
    };

    yield call(processRequest, 'profile/experience', 'POST', requestPayload);
    yield put(experienceActions.createExperienceSuccess());
    yield put(replace('/dashboard'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during updating experience!', true));
    yield put(experienceActions.createExperienceError(e));
  }
}

export function* handleGetExperience(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `profile/experience/${ id }`);

    yield put(experienceActions.getExperienceSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during getting experience!', true));
    yield put(experienceActions.getExperienceError(e));
  }
}

export function* handleUpdateExperience(action) {
  try {
    const { id, formData } = action.payload;
    const { title, company, location, from, to, current, description } = formData;

    const requestPayload = {
      title,
      company,
      location,
      from,
      to: current ? '' : to,
      current,
      description,
    };

    yield call(processRequest, `profile/experience/${ id }`, 'POST', requestPayload);
    yield put(experienceActions.updateExperienceSuccess());
    yield put(replace('/dashboard'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during creating experience!', true));
    yield put(experienceActions.createExperienceError(e));
  }
}

export function* handleDeleteExperience(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `profile/experience/${ id }`, 'DELETE');

    yield put(profileActions.getCurrentUserProfileSuccess(data));
    yield put(experienceActions.deleteExperienceSuccess());
  } catch(e) {
    yield put(notificationActions.createNotification('Error during deleting experience', true));
    yield put(experienceActions.deleteExperienceError(e));
  }
}