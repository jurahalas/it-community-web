import { experienceActionTypes } from './ExperienceConstants';

const initialState = {
  title: '',
  company: '',
  location: '',
  from: '',
  to: '',
  current: false,
  description: '',
  error: {
    error: '',
    status: '',
  },
  loading: false,
};

export default function experienceReducer(state = initialState, action) {
  switch(action.type) {

    case experienceActionTypes.GET_EXPERIENCE_REQUEST:
    case experienceActionTypes.CREATE_EXPERIENCE_REQUEST:
    case experienceActionTypes.UPDATE_EXPERIENCE_REQUEST:
    case experienceActionTypes.DELETE_EXPERIENCE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case experienceActionTypes.GET_EXPERIENCE_SUCCESS:
      return {
        ...state,
        ...action.payload.data[0],
        loading: false,
      };

    case experienceActionTypes.GET_EXPERIENCE_ERROR:
      return {
        ...state,
        error: {
          error: action.payload.errors,
          status: action.payload.errors.status,
        },
        loading: false,
      };

    case experienceActionTypes.CREATE_EXPERIENCE_SUCCESS:
    case experienceActionTypes.UPDATE_EXPERIENCE_SUCCESS:
    case experienceActionTypes.DELETE_EXPERIENCE_SUCCESS:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}