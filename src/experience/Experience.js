import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import './Experience.css';
import Notification from '../components/notification/Notification';
import CustomButton from '../components/customButton/CustomButton';
import Spinner from '../components/spinner/SpinnerGrid';
import { experienceValidate } from '../services/FormValidate';
import InputComponent from '../components/inputComponent/InputComponent';
import { connect } from 'react-redux';
import * as experienceActions from './ExperienceActions';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import Moment from 'moment';

class Experience extends Component {
  constructor() {
    super();
    this.state = {
      current: null,
    };
  }

  componentDidMount() {
    const { experienceActions, route: { id } } = this.props;

    if(id)
      experienceActions.getExperienceRequest(id);
  }

  onCheck=() => {
    const { current } = this.state;

    if(current === null)
      this.setState({
        current: false,
      });

    else
      this.setState({
        current: !current,
      });
  };

  renderField = ({ disabled, label, type, mask, meta: { error, touched, invalid }, input: { onChange, value, onBlur, onFocus } }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
      <InputComponent
        placeholder={ label }
        onChange={ onChange }
        value={ type === 'date' ? Moment(value).format('YYYY-MM-DD') : value }
        onBlur={ onBlur }
        onFocus={ onFocus }
        error={ error && touched }
        mask={ mask }
        type={ type }
        disabled={ disabled }
      />
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  onSubmit = (formData) => {
    const { experienceActions, route: { id } } = this.props;

    if(id) {
      experienceActions.updateExperienceRequest({ ...formData, current: this.state.current }, id);
      this.props.initialize(true);
    }
    else {
      experienceActions.createExperienceRequest({ ...formData, current: this.state.current });
      this.props.initialize(true);
    }
  };

  openDashboardPage = () =>
    this.props.history.push('/dashboard');

  render() {
    const { experienceReducer, handleSubmit, route: { id } } = this.props;
    const { current } = this.state;
    const checked = current || (current === null && experienceReducer.current);

    return (
      <div className="experience-container">
        <Spinner isShow={ experienceReducer.loading } />
        <Notification />
        <div className="experience-title-wrapper">
          <div className="experience-title">
            Experience
          </div>
          <div className='experience-buttons-container'>
            <CustomButton
              text='Cancel'
              clickHandler={ this.openDashboardPage }
              className="cancel"
              fontFamily="Quicksand"
              height={ 35 }
            />
            <CustomButton
              text='Save'
              clickHandler={ handleSubmit(this.onSubmit) }
              className="submit"
              fontFamily="Quicksand"
              height={ 35 }
            />
          </div>
        </div>
        <div className="experience-detail">
          <div className='header-form'>
            <h1>{ id ? 'Edit' : 'Add' } Experience</h1>
            <h3>Add any job or position that you have or had in the past or current</h3>
            <p>*= required fields</p>
          </div>
          <form className='experience-form' onSubmit={ handleSubmit(this.onSubmit) }>
            <div className='experience-block-item'>
              <Field
                name="title"
                type="text"
                component={ this.renderField }
                label="* Job Title"
              />
              <Field
                name="company"
                type="text"
                component={ this.renderField }
                label="* Company"
              />
              <Field
                name="location"
                type="text"
                component={ this.renderField }
                label="Location"
              />
              <h4>* From Date</h4>
              <Field
                name="from"
                type="date"
                component={ this.renderField }
                label="Enter data from"
              />
              {
                !checked &&
                <div>
                  <h4>* To Date</h4>
                  <Field
                    name="to"
                    type="date"
                    component={ this.renderField }
                    label="Enter data from"
                    disabled={ checked }
                  />
                </div>
              }
              <div className="form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="current"
                  value={ checked }
                  checked={ checked }
                  onChange={ this.onCheck }
                  id="current"
                />
                <label htmlFor="current" className="form-check-label">
                    Current Job
                </label>
              </div>
              <Field
                name="description"
                type="text"
                component={ this.renderField }
                label="Enter description"
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Experience = reduxForm({
  form: 'Experience',
  validate: experienceValidate,
  enableReinitialize: true,
})(Experience);

Experience.propTypes = {
  experienceActions: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
  history: PropTypes.object.isRequired,
  experienceReducer: PropTypes.object.isRequired,
  route: PropTypes.shape({
    id: PropTypes.string,
  }),
};

function mapStateToProps(state) {
  return {
    experienceReducer: state.experienceReducer,
    initialValues: state.experienceReducer,
    form: state.formReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    experienceActions: bindActionCreators(experienceActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Experience);