import bowser from 'bowser';

export const isIEReloadPage = () => (bowser.msie && window.location.reload());
