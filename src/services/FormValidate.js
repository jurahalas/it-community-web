import Validator from 'validator';
import { isEmpty } from './isEmpty';

export const signUpValidate = values => {
  const errors = {};

  if(!values.email)
    errors.email = 'Please enter your email';
  else if(!Validator.isEmail(values.email))
    errors.email = 'Invalid email address';

  if(!values.name)
    errors.name = 'Please enter your Name';

  if(!values.password)
    errors.password = 'Please enter your Password';
  if(values.password && values.password.length < 6)
    errors.password = 'Password length should be 6 or more ';

  if(values.password && !values.confirm_password)
    errors.confirm_password = 'Please enter Confirm Password';
  if(values.password && values.confirm_password && values.password !== values.confirm_password)
    errors.confirm_password = 'Passwords are not equal';

  if(!values.driver_license)
    errors.driver_license = 'Please enter your driver license';
  if(values.driver_license && values.driver_license.length > 50)
    errors.driver_license = 'driver license length should be less than 50 symbols ';

  return errors;
};

export const signInValidate = values => {
  const errors = {};

  if(!values.email)
    errors.email = 'Please enter your email';
  else if(!Validator.isEmail(values.email))
    errors.email = 'Invalid email address';
  if(!values.password)
    errors.password = 'Please enter your password';
  if(values.password && values.password.length < 6)
    errors.password = 'Password length should be 6 or more ';

  return errors;
};

export const profileValidate = values => {
  const errors = {};

  if(!values.handle)
    errors.handle = 'Please enter your nickname';

  if(!values.status)
    errors.status = 'Please enter your status';

  if(!values.skills)
    errors.skills = 'Please enter your skills';

  if(!isEmpty(values.website))
    if(!Validator.isURL(values.website)) {
      errors.website = 'Not a valid URL';
    }

  if(!isEmpty(values.youtube))
    if(!Validator.isURL(values.youtube)) {
      errors.youtube = 'Not a valid URL';
    }

  if(!isEmpty(values.twitter))
    if(!Validator.isURL(values.twitter)) {
      errors.twitter = 'Not a valid URL';
    }

  if(!isEmpty(values.facebook))
    if(!Validator.isURL(values.facebook)) {
      errors.facebook = 'Not a valid URL';
    }

  if(!isEmpty(values.linkedin))
    if(!Validator.isURL(values.linkedin)) {
      errors.linkedin = 'Not a valid URL';
    }

  if(!isEmpty(values.instagram))
    if(!Validator.isURL(values.instagram)) {
      errors.instagram = 'Not a valid URL';
    }

  return errors;
};

export const experienceValidate = values => {
  const errors = {};

  if(!values.title)
    errors.title = 'Please enter title';

  if(!values.company)
    errors.company = 'Please enter title';

  if(!values.from)
    errors.from = 'Please enter from date';

  if(!values.to)
    errors.to = 'Please enter to date';

  return errors;
};

export const educationValidate = values => {
  const errors = {};

  if(!values.school)
    errors.school = 'Please enter school';

  if(!values.degree)
    errors.degree = 'Please enter degree';

  if(!values.fieldOfStudy)
    errors.fieldOfStudy = 'Please enter fieldOfStudy';

  if(!values.from)
    errors.from = 'Please enter from date';

  if(!values.to)
    errors.to = 'Please enter to date';

  return errors;
};

export const createPostValidate = values => {
  const errors = {};

  if(!values.post)
    errors.post = 'Please enter post data';

  return errors;
};