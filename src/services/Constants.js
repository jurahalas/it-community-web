export const BASE_API_URL = 'http://localhost:5001/';
export const DATE_FORMAT_FOR_SIGN_IN = 'YYYY-MM-DD HH:mm:ss';
export const ACCEPTED_IMAGES_MIME_TYPES = [
  'image/png',
  'image/jpg',
  'image/jpeg',
  'image/pjpeg',
];
