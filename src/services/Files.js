export const filterFilesByMimeTypes = (files, mimeTypes, limit = 1) => {
  const result = [];

  for(let i = 0; i < files.length; i++) {
    const file = files[i];

    if(mimeTypes.indexOf(file.type) !== -1)
      result.push(file);

    if(result.length >= limit)
      return result;
  }

  return result;
};

export const getCanvasBlob = (image) => new Promise((resolve, reject) => {
  image.toBlob((blob) => {
    resolve(blob);
  }, 'image/jpeg');
});