import axios from 'axios';
import { BASE_API_URL } from './Constants';
import { push } from 'react-router-redux';
import * as signInActions from '../signIn/SignInActions';
import Store from '../store/Store';

export function checkStatus(responseData) {
  if(responseData.status >= 200 && responseData.status < 300)
    return { data: responseData.data, headers: responseData.headers, status: responseData.status };

  const error = new Error((responseData.data.response && responseData.data.response.message) || '');

  error.response = responseData.data;
  throw error;
}

export function processRequest(url = '', method = 'GET', data = {}, handleCatch = true, file = false) {
  const access_token = localStorage.getItem('access_token');
  let headers = {
    'Content-Type': 'application/json',
  };

  if(url !== 'users/register' || url !== 'users/login')
    headers = { ...headers, 'Authorization': `Bearer ${access_token}` };

  return axios({
    method,
    data: file ? data : JSON.stringify(data),
    headers,
    crossDomain: true,
    url: BASE_API_URL + url,
  }).then(res => checkStatus(res)
  ).catch(err => {
    if(!handleCatch)
      throw err;

    if(err.response !== null && err.response.status === 401 && err.response.statusText === 'Unauthorized') {
      Store.dispatch(signInActions.logout());
      Store.dispatch(push('/'));
    }

    throw err;
  });
}