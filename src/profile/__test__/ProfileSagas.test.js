import * as sagas from '../ProfileSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as profileActions from '../ProfileActions';
import * as notificationActions from '../../components/notification/NotificationActions';
import { replace } from 'react-router-redux';

describe('Get user profiles data', () => {
  const action = {
  };

  it('Should successfully get all profiles data', () => {
    const generator = sagas.handleProfiles(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile/all'));

    next = generator.next({ data: {} });
    expect(next.value).toEqual(put(profileActions.getProfilesSuccess({})));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleProfiles(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile/all'));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put((notificationActions.createNotification(`Can't get user profile data!`, true))));

    next = generator.next();
    expect(next.value).toEqual(put(profileActions.getCurrentUserProfileError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

describe('Get user profile data', () => {
  const action = {
    payload: {
      handle: 'testtt',
    },
  };

  it('Should successfully get user profile data', () => {
    const generator = sagas.handleGetUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, `profile/handle/${ action.payload.handle }`));

    next = generator.next({ data: {} });
    expect(next.value).toEqual(put(profileActions.getUserProfileSuccess({})));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleGetUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, `profile/handle/${ action.payload.handle }`));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put((notificationActions.createNotification(`Can't get user profile data!`, true))));

    next = generator.next();
    expect(next.value).toEqual(put(profileActions.getUserProfileError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

describe('Get current user profile data', () => {
  const action = {
    payload: {
      handle: 'testtt',
    },
  };

  it('Should successfully get user profile data', () => {
    const generator = sagas.handleGetCurrentUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile'));

    next = generator.next({ data: {} });
    expect(next.value).toEqual(put(profileActions.getCurrentUserProfileSuccess({})));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleGetCurrentUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile'));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put((notificationActions.createNotification(`Can't get user profile data!`, true))));

    next = generator.next();
    expect(next.value).toEqual(put(profileActions.getCurrentUserProfileError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});

describe('Update user profile', () => {
  const action = {
    payload: {
      data: {
        name: 'test',
        handle: 'admin@eboxsecure.com',
        company: 'active',
        website: 'admin',
        location: 'admin',
        status: 'testtt',
        skills: ['testtt'],
        bio: null,
        githubUsername: null,
        bitbucketUsername: null,
        google: '',
        twitter: '',
        facebook: '',
        linkedin: '',
      },
    },
  };

  const { handle, company, website, location, status, skills, bio, githubUsername, bitbucketUsername, name, google, twitter, facebook, linkedin } = action.payload.data;

  const requestPayload = {
    name,
    handle,
    company,
    website,
    location,
    status,
    skills,
    bio,
    githubUsername,
    bitbucketUsername,
    google,
    twitter,
    facebook,
    linkedin,
  };

  it('Should successfully update user profile', () => {
    const generator = sagas.handleUpdateUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile', 'POST', requestPayload));

    next = generator.next({ data: {} });
    expect(next.value).toEqual(put(profileActions.updateUserProfileSuccess({})));

    next = generator.next();
    expect(next.value).toEqual(put(replace('/my-profile')));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should failed on error', () => {
    const generator = sagas.handleUpdateUserProfile(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'profile', 'POST', requestPayload));

    next = generator.throw(new Error('500 Internal Server Error'));
    expect(next.value).toEqual(put((notificationActions.createNotification(`Error during updated user profile!`, true))));

    next = generator.next();
    expect(next.value).toEqual(put(profileActions.updateUserProfileError(new Error('500 Internal Server Error'))));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});
