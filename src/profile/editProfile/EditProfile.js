import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import './EditProfile.css';
import Notification from '../../components/notification/Notification';
import CustomButton from '../../components/customButton/CustomButton';
import Spinner from '../../components/spinner/SpinnerGrid';
import { profileValidate } from '../../services/FormValidate';
import InputComponent from '../../components/inputComponent/InputComponent';
import { connect } from 'react-redux';
import * as profileActions from '../ProfileActions';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import ProfileImageBlock from '../profileImageBlock/ProfileImageBlock';

class EditProfile extends Component {
  componentDidMount() {
    this.props.profileActions.getCurrentUserProfileRequest();
  }

  renderField = ({ label, type, mask, meta: { error, touched, invalid }, input: { onChange, value, onBlur, onFocus } }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
      { type !== 'phone_number' && type !== 'date' &&
        <InputComponent
          placeholder={ label }
          onChange={ onChange }
          value={ value }
          onBlur={ onBlur }
          onFocus={ onFocus }
          error={ error && touched }
          mask={ mask }
        />
      }
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  onSubmit = (formData) => {
    this.props.profileActions.updateUserProfileRequest(formData);
  };

  openProfilePage = () =>
    this.props.history.push('/my-profile');

  render() {
    const { profileData, handleSubmit } = this.props;

    return (
      <div className="edit-profile-container">
        <Spinner isShow={ profileData.loading } />
        <Notification />
        <div className="edit-profile-title-wrapper">
          <div className="edit-profile-title">
            Edit Profile
          </div>
          <div className='edit-profile-buttons-container'>
            <CustomButton
              text='Cancel'
              clickHandler={ this.openProfilePage }
              className="cancel-btn"
              fontFamily="Quicksand"
              height={ 35 }
              color='#005777'
            />
            <CustomButton
              text='Save'
              clickHandler={ handleSubmit(this.onSubmit) }
              className="submit-btn"
              fontFamily="Quicksand"
              height={ 35 }
            />
          </div>
        </div>
        <div className="edit-profile-detail">
          <ProfileImageBlock />
          <div className="edit-profile">
            <form onSubmit={ handleSubmit(this.onSubmit) }>
              <div className='edit-profile-block-item'>
                <Field
                  name="name"
                  type="text"
                  component={ this.renderField }
                  label="Enter your first and last name"
                />
                <Field
                  name="handle"
                  type="text"
                  component={ this.renderField }
                  label="Enter your nickname"
                />
                <Field
                  name="company"
                  type="text"
                  component={ this.renderField }
                  label="Enter your company"
                />
                <Field
                  name="website"
                  type="email"
                  component={ this.renderField }
                  label="Enter your website"
                />
                <Field
                  name="location"
                  type="text"
                  component={ this.renderField }
                  label="Enter your location"
                />
                <Field
                  name="status"
                  type="text"
                  component={ this.renderField }
                  label="Enter your position"
                />
                <Field
                  name="bio"
                  type="text"
                  component={ this.renderField }
                  label="Enter your bio"
                />
                <Field
                  name="skills"
                  type="text"
                  component={ this.renderField }
                  label="Enter your skills"
                />
                <Field
                  name="githubUsername"
                  type="text"
                  component={ this.renderField }
                  label="Enter your githubUsername"
                />
                <Field
                  name="bitbucketUsername"
                  type="text"
                  component={ this.renderField }
                  label="Enter your bitbucketUsername"
                />
              </div>
              <div className='social-media-block-items'>
                <div className='social-media-title'>Your social media</div>
                <Field
                  name="google"
                  type="text"
                  component={ this.renderField }
                  label="Enter your gmail"
                />
                <Field
                  name="twitter"
                  type="text"
                  component={ this.renderField }
                  label="Enter your twitter"
                />
                <Field
                  name="facebook"
                  type="text"
                  component={ this.renderField }
                  label="Enter your facebook"
                />
                <Field
                  name="linkedin"
                  type="text"
                  component={ this.renderField }
                  label="Enter your linkedin"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

EditProfile = reduxForm({
  form: 'EditProfile',
  validate: profileValidate,
  enableReinitialize: true,
})(EditProfile);

EditProfile.propTypes = {
  profileActions: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func,
  history: PropTypes.object.isRequired,
  profileData: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
    initialValues: state.profileReducer.userData,
    form: state.formReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);