import React, { Component } from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

class ProfileCard extends Component {
  render() {
    const { experience, education } = this.props;

    const expItems = experience && experience.map(exp => (
      <li key={ exp._id }>
        <h4>{ exp.company }</h4>
        <p>
          <Moment format="YYYY/MM/DD">{ exp.from }</Moment> -
          { exp.to === null ? (
            ' Now'
          ) : (
            <Moment format="YYYY/MM/DD">{ exp.to }</Moment>
          ) }
        </p>
        <p>
          <strong>Position:</strong> { exp.title }
        </p>
        <p>
          { exp.location === '' ? null : (
            <span>
              <strong>Location: </strong> { exp.location }
            </span>
          ) }
        </p>
        <p>
          { exp.description === '' ? null : (
            <span>
              <strong>Description: </strong> { exp.description }
            </span>
          ) }
        </p>
      </li>
    ));

    const eduItems = education && education.map(edu => (
      <li key={ edu._id }>
        <h4>{ edu.school }</h4>
        <p>
          <Moment format="YYYY/MM/DD">{ edu.from }</Moment> -
          { edu.to === null ? (
            ' Now'
          ) : (
            <Moment format="YYYY/MM/DD">{ edu.to }</Moment>
          ) }
        </p>
        <p>
          <strong>Degree:</strong> { edu.degree }
        </p>
        <p>
          <strong>Field Of Study:</strong> { edu.fieldofstudy }
        </p>
        <p>
          { edu.description === '' ? null : (
            <span>
              <strong>Description: </strong> { edu.description }
            </span>
          ) }
        </p>
      </li>
    ));

    return (
      <div className="profile-card-container">
        <div className="profile-card-wrapper">
          <h3>Experience</h3>
          { expItems && expItems.length > 0 ? (
            <ul className="profile-list-group">{ expItems && expItems }</ul>
          ) : (
            <h4>No Experience Listed</h4>
          ) }
        </div>

        <div className="profile-card-wrapper">
          <h3>Education</h3>
          { eduItems && eduItems.length > 0 ? (
            <ul className="profile-list-group">{ eduItems && eduItems }</ul>
          ) : (
            <h4>No Education Listed</h4>
          ) }
        </div>
      </div>
    );
  }
}

ProfileCard.propTypes = {
  experience: PropTypes.object,
  education: PropTypes.object,
};

export default ProfileCard;
