import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { isEmpty } from '../../../services/isEmpty';
import userAvatar from '../../../images/avatar.svg';
import './ProfileItem.css';
import checkIcon from '../../../images/checkmark.svg';

class ProfileItem extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="profile-item-container">
        <div className="personal-info-container">
          <div className='personal-info'>
            <img src={ profile.user.avatar || userAvatar } alt="" className="rounded-circle" />
            <div className='profile-info-block'>
              <h3>{ profile.user.name }</h3>
              <p>
                { profile.status }{ ' ' }
                { isEmpty(profile.company) ? null : (
                  <span>at { profile.company }</span>
                ) }
              </p>
              <p>{ isEmpty(profile.location) ? null : (
                <span>{ profile.location }</span>
              ) }
              </p>
              <Link to={ `/profile/${profile.handle}` } className="link">
                View Profile
              </Link>
            </div>

          </div>
          <div className='skills-container'>
            <h4>Skill Set</h4>
            <ul className="list-group">
              { profile.skills.slice(0, 4).map((skill, index) => (
                <li key={ index } className="list-group-item">
                  <img src={ checkIcon } className='skill' alt='' />
                  { skill }
                </li>
              )) }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired,
};

export default ProfileItem;
