import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProfileItem from './profileItem/ProfileItem';
import * as profileActions from '../ProfileActions';
import { bindActionCreators } from 'redux';
import './Profile.css';
import Spinner from '../../components/spinner/SpinnerGrid';
import classNames from 'classnames';

class Profiles extends Component {
  componentDidMount() {
    this.props.profileActions.getProfilesRequest();
  }

  render() {
    const { profiles, loading } = this.props.profileData;

    let profileItems;

    if(profiles && profiles.length > 0)
      profileItems = profiles.map(profile => (
        <ProfileItem key={ profile._id } profile={ profile } />
      ));
    else
      profileItems = <h4>No profiles found...</h4>;

    return (
      <div className={ classNames('profiles-container', { 'no-content': profiles === null || (profiles.length && profiles.length === 0) }) }>
        <Spinner isShow={ loading } />
        <h1>Developer Profiles</h1>
        <h4>Browse and connect with developers</h4>
        { profileItems }
      </div>
    );
  }
}

Profiles.propTypes = {
  profileActions: PropTypes.object,
  profileData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profiles);