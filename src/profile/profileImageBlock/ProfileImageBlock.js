import React, { Component } from 'react';
import './ProfileImageBlock.css';
import userAvatar from '../../images/avatar.svg';
import CusttomBottom from '../../components/customButton/CustomButton';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from '../ProfileActions';

import AvatarUpload from '../../components/modals/avatarUpload/AvatarUpload';
import { getCanvasBlob } from '../../services/Files';

class ProfileImageBlock extends Component {
  handleOpenChangePasswordModal = () => { };

  handleDelete = () =>
    this.props.profileActions.deleteUserProfileRequest();

  handleSubmitAvatarChange = async(image, name) => {
    const canvasBlob = await getCanvasBlob(image);
    const data = new FormData();

    data.append('file', canvasBlob, name);

    this.props.profileActions.uploadProfileAvatarRequest(data);
  };

  handleOpenAvatarUploadModal = () => {
    this.props.profileActions.setAvatarModal(true);
  };

  handleCloseAvatarUploadModal = () => {
    this.props.profileActions.setAvatarModal(false);
  };

  render() {
    const { profileData } = this.props;
    const { userData, modalAvatarUpload, error } = profileData;
    const { user, handle } = userData;

    return (

      <div className="profile-user-image-block">
        <img src={ user.avatar || localStorage.googleAvatar || userAvatar } alt="" className="profile-image" />
        {
          (localStorage.handle === handle || error.status === 'NonExist') &&
          <div className="profile-btn-wrapper">
            <CusttomBottom
              text='Upload Img'
              className='image-block-btn'
              color='#005777'
              height={ 15 }
              width={ 160 }
              clickHandler={ this.handleOpenAvatarUploadModal }
            />
            <CusttomBottom
              text='Change password'
              className='image-block-btn'
              color='#005777'
              height={ 15 }
              width={ 160 }
              clickHandler={ this.handleOpenChangePasswordModal }
            />
            <CusttomBottom
              text='Delete my accout'
              className='image-block-btn'
              color='#005777'
              height={ 15 }
              width={ 160 }
              clickHandler={ this.handleDelete }
            />
          </div>
        }
        <AvatarUpload
          open={ modalAvatarUpload.open }
          loading={ modalAvatarUpload.loading }
          onCancel={ this.handleCloseAvatarUploadModal }
          onSubmit={ this.handleSubmitAvatarChange }
        />
      </div>
    );
  }
}

ProfileImageBlock.propTypes = {
  profileActions: PropTypes.object,
  profileData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    profileData: state.profileReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileImageBlock);
