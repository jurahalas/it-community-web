import { userProfileActionTypes } from './ProfileContants';

const initialState = {
  google: '',
  linkedin: '',
  twitter: '',
  facebook: '',
  profiles: null,
  userData: {
    user: {
      name: '',
      avatar: '',
    },
    name: '',
    handle: '',
    company: '',
    website: '',
    location: '',
    status: '',
    skills: '',
    bio: '',
    githubUsername: '',
    bitbucketUsername: '',
    social: {
      google: '',
      linkedin: '',
      twitter: '',
      facebook: '',
    },
    experience: null,
    education: null,
  },
  error: {
    error: '',
    status: '',
  },
  loading: false,
  modalAvatarUpload: {
    open: false,
    loading: false,
  },
};

export default function profileReducer(state = initialState, action) {
  const { type, payload } = action;
  const { data, errors, error, status, loading, dataAvatar } = payload || {};

  switch(type) {

    case userProfileActionTypes.GET_PROFILES_REQUEST:
    case userProfileActionTypes.DELETE_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case userProfileActionTypes.GET_PROFILES_SUCCESS:
      return {
        ...state,
        profiles: data,
        loading: false,
      };

    case userProfileActionTypes.GET_PROFILES_ERROR:
      return {
        ...state,
        error: {
          error: errors,
          status: errors.status,
        },
        loading: false,
      };

    case userProfileActionTypes.GET_CURRENT_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case userProfileActionTypes.GET_CURRENT_USER_PROFILE_SUCCESS:
      localStorage.handle = data.handle;

      const { google, linkedin, facebook, twitter } = data.social || {};

      return {
        ...state,
        userData: {
          ...data,
          google,
          linkedin,
          twitter,
          facebook,
          name: data.user.name,
        },
        loading: false,
      };

    case userProfileActionTypes.GET_USER_PROFILE_ERROR:
      const { user } = errors || '';

      return {
        ...initialState,
        error: {
          error: errors,
          status: errors.status,
        },
        loading: false,
        userData: {
          ...state.userData,
          name: user,
        },
      };

    case userProfileActionTypes.GET_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case userProfileActionTypes.GET_USER_PROFILE_SUCCESS:
      return {
        ...state,
        userData: {
          ...data,
          google: (data.social && data.social.google) || '',
          linkedin: (data.social && data.social.linkedin) || '',
          twitter: (data.social && data.social.twitter) || '',
          facebook: (data.social && data.social.facebook) || '',
        },
        error: {
          error: '',
          status: '',
        },
        loading: false,
      };

    case userProfileActionTypes.GET_CURRENT_USER_PROFILE_ERROR:
      return {
        ...state,
        error: {
          error: errors,
          status: errors.status,
        },
        loading: false,
      };

    case userProfileActionTypes.UPDATE_USER_PROFILE_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case userProfileActionTypes.UPDATE_USER_PROFILE_SUCCESS:
      localStorage.handle = data.handle;

      return {
        ...state,
        userData: data,
        loading: false,
        error: {
          error: '',
          status: '',
        },
      };

    case userProfileActionTypes.UPDATE_USER_PROFILE_ERROR:
    case userProfileActionTypes.DELETE_PROFILE_ERROR:
      return {
        ...state,
        error: error.message,
        loading: false,
      };

    case userProfileActionTypes.DELETE_PROFILE_SUCCESS:
      localStorage.removeItem('access_token');
      localStorage.removeItem('email');
      localStorage.removeItem('handle');
      localStorage.removeItem('name');

      return initialState;

    case userProfileActionTypes.SET_AVATAR_MODAL: {
      return {
        ...state,
        modalAvatarUpload: {
          open: status,
          loading,
        },
      };
    }

    case userProfileActionTypes.UPLOAD_PROFILE_AVATAR_SUCCESS: {
      localStorage.avatar = dataAvatar.avatar;

      return {
        ...state,
        userData: {
          ...state.userData,
          user: {
            ...state.userData.user,
            avatar: dataAvatar.avatar,
          },

        },
      };
    }

    default:
      return state;
  }
}