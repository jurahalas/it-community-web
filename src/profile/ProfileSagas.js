import { processRequest } from '../services/Api';
import { all, takeEvery, takeLatest } from 'redux-saga/effects';
import { put, call } from 'redux-saga/effects';
import { replace } from 'react-router-redux';
import { userProfileActionTypes } from '../profile/ProfileContants';
import * as profileActions from '../profile/ProfileActions';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeLatest(userProfileActionTypes.GET_PROFILES_REQUEST, handleProfiles),
    yield takeLatest(userProfileActionTypes.GET_USER_PROFILE_REQUEST, handleGetUserProfile),
    yield takeLatest(userProfileActionTypes.GET_CURRENT_USER_PROFILE_REQUEST, handleGetCurrentUserProfile),
    yield takeLatest(userProfileActionTypes.UPDATE_USER_PROFILE_REQUEST, handleUpdateUserProfile),
    yield takeLatest(userProfileActionTypes.DELETE_PROFILE_REQUEST, handleDeleteProfile),
    yield takeEvery(userProfileActionTypes.UPLOAD_PROFILE_AVATAR_REQUEST, handlePostAvatar),
  ]);
}

export function* handleProfiles() {
  try {
    const { data } = yield call(processRequest, 'profile/all');

    yield put(profileActions.getProfilesSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get user profile data!`, true));
    yield put(profileActions.getCurrentUserProfileError(e));
  }
}

export function* handleGetUserProfile(action) {
  try {
    const { handle } = action.payload;

    const { data } = yield call(processRequest, `profile/handle/${ handle }`);

    if(data.noprofile) {
      yield put(notificationActions.createNotification(`${ data.noprofile }`, false));
      yield put(profileActions.getUserProfileError(data));
    }
    else
      yield put(profileActions.getUserProfileSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get user profile data!`, true));
    yield put(profileActions.getUserProfileError(e));
  }
}

export function* handleGetCurrentUserProfile() {
  try {
    const { data } = yield call(processRequest, 'profile');

    if(data.noprofile) {
      yield put(notificationActions.createNotification(`${ data.noprofile }`, false));
      yield put(profileActions.getCurrentUserProfileError(data));
    }
    else
      yield put(profileActions.getCurrentUserProfileSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get user profile data!`, true));
    yield put(profileActions.getCurrentUserProfileError(e));
  }
}

export function* handleUpdateUserProfile(action) {
  try {
    const { handle, company, website, location, status, skills, bio, githubUsername, bitbucketUsername, google, twitter, facebook, linkedin, name } = action.payload.data;
    const requestPayload = {
      name,
      handle,
      company,
      website,
      location,
      status,
      skills: typeof skills === 'object' ? skills : skills.split(','),
      bio,
      githubUsername,
      bitbucketUsername,
      google,
      twitter,
      facebook,
      linkedin,
    };

    const { data } = yield call(processRequest, 'profile', 'POST', requestPayload);

    yield put(profileActions.updateUserProfileSuccess(data));
    yield put(replace('/my-profile'));
  } catch(e) {
    yield put(notificationActions.createNotification(`Error during updated user profile!`, true));
    yield put(profileActions.updateUserProfileError(e));
  }
}

export function* handleDeleteProfile() {
  try {
    yield call(processRequest, 'profile', 'DELETE');
    yield put(profileActions.deleteUserProfileSuccess());
    yield put(replace('/'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during deleting profile', true));
    yield put(profileActions.deleteUserProfileError(e));
  }
}

export function* handlePostAvatar(action) {
  try {
    yield put(profileActions.setAvatarModal(true, true));
    const { data } = yield call(processRequest, 'profile/images', 'POST', action.payload.dataAvatar, true, true);

    yield put(profileActions.uploadProfileAvatarSuccess(data));
    yield put(profileActions.setAvatarModal(false));
  } catch(e) {
    yield put(profileActions.setAvatarModal(false));
    //yield call(handleError, e, 'Error during uploading avatar. Please try again later.');
  }
}
