import { userProfileActionTypes } from './ProfileContants';

export function getProfilesRequest() {
  return {
    type: userProfileActionTypes.GET_PROFILES_REQUEST,
  };
}

export function getProfilesSuccess(data) {
  return {
    type: userProfileActionTypes.GET_PROFILES_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getProfilesError(errors) {
  return {
    type: userProfileActionTypes.GET_PROFILES_ERROR,
    payload: {
      errors,
    },
  };
}

export function getCurrentUserProfileRequest() {
  return {
    type: userProfileActionTypes.GET_CURRENT_USER_PROFILE_REQUEST,
  };
}

export function getCurrentUserProfileSuccess(data) {
  return {
    type: userProfileActionTypes.GET_CURRENT_USER_PROFILE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getCurrentUserProfileError(errors) {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_ERROR,
    payload: {
      errors,
    },
  };
}

export function getUserProfileRequest(handle) {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_REQUEST,
    payload: {
      handle,
    },
  };
}

export function getUserProfileSuccess(data) {
  return {
    type: userProfileActionTypes.GET_USER_PROFILE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getUserProfileError(errors) {
  return {
    type: userProfileActionTypes.GET_CURRENT_USER_PROFILE_ERROR,
    payload: {
      errors,
    },
  };
}

export function updateUserProfileRequest(data) {
  return {
    type: userProfileActionTypes.UPDATE_USER_PROFILE_REQUEST,
    payload: {
      data,
    },
  };
}

export function updateUserProfileSuccess(data) {
  return {
    type: userProfileActionTypes.UPDATE_USER_PROFILE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function updateUserProfileError(errors) {
  return {
    type: userProfileActionTypes.UPDATE_USER_PROFILE_ERROR,
    payload: {
      errors,
    },
  };
}

export function deleteUserProfileRequest(data) {
  return {
    type: userProfileActionTypes.DELETE_PROFILE_REQUEST,
    payload: {
      data,
    },
  };
}

export function deleteUserProfileSuccess() {
  return {
    type: userProfileActionTypes.DELETE_PROFILE_SUCCESS,
  };
}

export function deleteUserProfileError(errors) {
  return {
    type: userProfileActionTypes.DELETE_PROFILE_ERROR,
    payload: {
      errors,
    },
  };
}

export function setAvatarModal(status, loading = false) {
  return {
    type: userProfileActionTypes.SET_AVATAR_MODAL,
    payload: {
      status,
      loading,
    },
  };
}

export function uploadProfileAvatarRequest(dataAvatar) {
  return {
    type: userProfileActionTypes.UPLOAD_PROFILE_AVATAR_REQUEST,
    payload: {
      dataAvatar,
    },
  };
}

export function uploadProfileAvatarSuccess(dataAvatar) {
  return {
    type: userProfileActionTypes.UPLOAD_PROFILE_AVATAR_SUCCESS,
    payload: {
      dataAvatar,
    },
  };
}
