import React, { Component } from 'react';
import './Profile.css';
import Notification from '../components/notification/Notification';
import CusttomBottom from '../components/customButton/CustomButton';
import PropTypes from 'prop-types';
import Spinner from '../components/spinner/SpinnerGrid';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from './ProfileActions';
import checkIcon from '../images/checkmark.svg';
import facebookIcon from '../images/facebook.svg';
import googleIcon from '../images/googleplus.svg';
import twitterIcon from '../images/twitter.svg';
import linkedinIcon from '../images/linkedin.svg';
import { isEmpty } from '../services/isEmpty';
import ProfileCard from './ProfileCard';
import ProfileImageBlock from './profileImageBlock/ProfileImageBlock';

class Profile extends Component {
  componentDidMount() {
    const { profileActions, route: { handle } } = this.props;

    if(handle)
      profileActions.getUserProfileRequest(handle);

    else
      profileActions.getCurrentUserProfileRequest();
  }

  handleEditClick = () =>
    this.props.history.push('/edit-profile');

  handleBackClick = () =>
    this.props.history.push('/profiles');

  handleCreateClick = () =>
    this.props.history.push('/create-profile');

  render() {
    const { profileData } = this.props;
    const { loading, userData } = profileData;
    const { google, linkedin, twitter, facebook } = userData.social || {};

    return (
      <div className="profile-container">
        <Spinner isShow={ loading } />
        <Notification />
        { profileData.error.status === 'NonExist'
          ? (
            <div className='profile-non-exist'>
              <h3>You have not yet setup a profile, please add some info</h3>
              <CusttomBottom
                text='Create Profile'
                className='profile-edit-btn'
                color='#ffffff'
                clickHandler={ this.handleCreateClick }
              />
            </div>
          )
          : (
            <div>
              <div className="profile-title-wrapper">
                <div className="profile-title">
                  { localStorage.handle === userData.handle ? 'My Profile' : 'User Profile' }
                </div>
                <CusttomBottom
                  text={ localStorage.handle === userData.handle ? 'Edit' : 'Back to all profiles' }
                  className='profile-edit-btn'
                  color='#ffffff'
                  clickHandler={ localStorage.handle === userData.handle ? this.handleEditClick : this.handleBackClick }
                />
              </div>
              <div className="profile-detail">
                <div className='profile-wrapper'>
                  <ProfileImageBlock />
                  <div className="profile-user-description">
                    <div className="profile-user-name">
                      { userData.user && `${userData.user.name}` }
                    </div>
                    <div className="detailed-description">
                      {
                        userData && userData.status &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Status</span>
                          </div>
                          <div className="detailed-value">
                            { userData.status }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.location &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Location</span>
                          </div>
                          <div className="detailed-value">
                            { userData.location }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.bio &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Bio</span>
                          </div>
                          <div className="detailed-value">
                            { userData.bio }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.company &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Company</span>
                          </div>
                          <div className="detailed-value">
                            { userData.company }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.website &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Website</span>
                          </div>
                          <div className="detailed-value">
                            { userData.website }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.githubUsername &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Github</span>
                          </div>
                          <div className="detailed-value">
                            { userData.githubUsername }
                          </div>
                        </div>
                      }
                      {
                        userData && userData.bitbucketUsername &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Bitbucket</span>
                          </div>
                          <div className="detailed-value">
                            { userData.bitbucketUsername }
                          </div>
                        </div>
                      }
                      {
                        !isEmpty(userData.skills) &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Skils</span>
                          </div>
                          <div className="detailed-value">
                            { userData.skills.slice(0, 4).map((skill, index) => (
                              <li key={ index } className="list-group-item">
                                <img src={ checkIcon } className='skill' alt='' />
                                { skill }
                              </li>
                            )) }
                          </div>
                        </div>
                      }
                      {
                        userData.social &&
                        <div className="detailed-item">
                          <div className="detailed-title">
                            <span className="detailed-title-name">Social media</span>
                          </div>
                          <div className="detailed-value">
                            { !isEmpty(facebook) &&
                            <a href={ facebook }>
                              <img src={ facebookIcon } className='social-media' alt='' />
                            </a>
                            }
                            { !isEmpty(google) &&
                            <a href={ google }>
                              <img src={ googleIcon } className='social-media' alt='' />
                            </a>
                            }
                            { !isEmpty(twitter) &&
                            <a href={ twitter }>
                              <img src={ twitterIcon } className='social-media' alt='' />
                            </a>
                            }
                            { !isEmpty(linkedin) &&
                            <a href={ linkedin }>
                              <img src={ linkedinIcon } className='social-media' alt='' />
                            </a>
                            }
                          </div>
                        </div>
                      }
                    </div>
                  </div>
                </div>

                <ProfileCard
                  education={ userData.education }
                  experience={ userData.experience }
                />
              </div>
            </div>
          )
        }
      </div>
    );
  }
}

Profile.propTypes = {
  profileActions: PropTypes.object,
  profileData: PropTypes.object,
  history: PropTypes.object,
  route: PropTypes.shape({
    handle: PropTypes.string,
  }),
};

function mapStateToProps(state) {
  return {
    ...state,
    profileData: state.profileReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
