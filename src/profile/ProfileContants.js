import keyMirror from 'keymirror';

export const userProfileActionTypes = keyMirror(
  {
    GET_PROFILES_REQUEST: null,
    GET_PROFILES_SUCCESS: null,
    GET_PROFILES_ERROR: null,
    GET_CURRENT_USER_PROFILE_REQUEST: null,
    GET_CURRENT_USER_PROFILE_SUCCESS: null,
    GET_CURRENT_USER_PROFILE_ERROR: null,
    GET_USER_PROFILE_REQUEST: null,
    GET_USER_PROFILE_SUCCESS: null,
    GET_USER_PROFILE_ERROR: null,
    UPDATE_USER_PROFILE_REQUEST: null,
    UPDATE_USER_PROFILE_SUCCESS: null,
    UPDATE_USER_PROFILE_ERROR: null,
    DELETE_PROFILE_REQUEST: null,
    DELETE_PROFILE_SUCCESS: null,
    DELETE_PROFILE_ERROR: null,
    SET_AVATAR_MODAL: null,
    UPLOAD_PROFILE_AVATAR_REQUEST: null,
    UPLOAD_PROFILE_AVATAR_SUCCESS: null,
  }
);