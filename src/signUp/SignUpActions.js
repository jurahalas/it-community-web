import { signUpActionTypes } from './SignUpConstants';

export function acceptTermsToggle(status) {
  return {
    type: signUpActionTypes.SIGN_UP_TERMS_ACCEPT,
    payload: {
      checked: status,
    },
  };
}

export function signUpRequest(formData) {
  return {
    type: signUpActionTypes.SIGN_UP_REQUEST,
    payload: {
      formData,
    },
  };
}

export function registerSuccess(token) {
  return {
    type: signUpActionTypes.SIGN_UP_SUCCESS,
    payload: {
      token,
    },
  };
}

export function registerError(error) {
  return {
    type: signUpActionTypes.REGISTER_ERROR,
    error: true,
    payload: {
      error,
    },
  };
}

export function cleanStateData() {
  return {
    type: signUpActionTypes.CLEAN_STATE_DATA,
  };
}