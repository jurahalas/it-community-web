import * as sagas from '../SignUpSagas';
import { processRequest } from '../../services/Api';
import { put, call } from 'redux-saga/effects';
import * as signUpActions from '../SignUpActions';
import * as notificationActions from '../../components/notification/NotificationActions';
import { replace } from 'react-router-redux';

describe('SignUp login request tests', () => {
  const action = {
    payload: {
      formData: {
        name: 'test',
        email: 'asd1@asd.com',
        password: '123456',
      },
    },
  };

  const error = {
    ...new Error('500 Internal Server Error'),
    response: { data: { errors: { email: { message: 'This email is already taken' } } } },
  };

  const requestPayload = {
    name: action.payload.formData.name,
    email: action.payload.formData.email,
    password: action.payload.formData.password,
  };

  it('Should successfully done login request saga (toke is exist)', () => {
    const responseMock = {
      data: {
        data: {
          user: {
            token: 'token',
          },
        },
      },
    };
    const generator = sagas.handleRegisterRequest(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'users/register', 'POST', requestPayload));

    next = generator.next(responseMock.data);
    expect(next.value).toEqual(put(signUpActions.registerSuccess(responseMock.data.data.user)));

    next = generator.next();
    expect(next.value).toEqual(put(replace('/')));

    next = generator.next();
    expect(next.done).toEqual(true);
  });

  it('Should fails when try to register user', () => {
    const generator = sagas.handleRegisterRequest(action);

    let next = generator.next();

    expect(next.value).toEqual(call(processRequest, 'users/register', 'POST', requestPayload));

    next = generator.throw(error);
    expect(next.value).toEqual(put(notificationActions.createNotification('User with provided email already exist!', true)));

    next = generator.next();
    expect(next.value).toEqual(put(signUpActions.registerError('This email is already taken')));

    next = generator.next();
    expect(next.done).toEqual(true);
  });
});
