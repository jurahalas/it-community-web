import React from 'react';
import SignUp from '../SignUp';
import { Provider } from 'react-redux';
import Store from '../../store/Store';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');

  shallow(
    <Provider store={ Store }>
      <SignUp
        router={ {} }
        signUpActions={ { formSubmit: jest.fn() } }
        signUpReducer={ {} }
      />
    </Provider>
    , div);
});
