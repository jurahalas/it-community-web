import { processRequest } from '../services/Api';
import { put, call, all, takeLatest } from 'redux-saga/effects';
import { signUpActionTypes } from './SignUpConstants';
import * as signUpActions from './SignUpActions';
import * as notificationActions from '../components/notification/NotificationActions';
import { replace } from 'react-router-redux';

export default function* () {
  yield all([
    yield takeLatest(signUpActionTypes.SIGN_UP_REQUEST, handleRegisterRequest),
  ]);
}

export function* handleRegisterRequest(action) {
  try {
    const { name, email, password } = action.payload.formData;

    const requestPayload = {
      name,
      email,
      password,
    };

    const data = yield call(processRequest, 'users/register', 'POST', requestPayload);

    if(data.data.user && data.data.user.token) {
      yield put(signUpActions.registerSuccess(data.data.user));
      yield put(replace('/'));
    }
  } catch(e) {
    const errorCode = e.response.data.errors.email.message;

    if(errorCode && errorCode === 'This email is already taken')
      yield put(notificationActions.createNotification('User with provided email already exist!', true));
    else
      yield put(notificationActions.createNotification('Error during creating user!', true));
    yield put(signUpActions.registerError(e.response.data.errors.email.message));
  }
}