import keyMirror from 'keymirror';

export const signUpActionTypes = keyMirror(
  {
    SIGN_UP_TERMS_ACCEPT: null,
    SIGN_UP_REQUEST: null,
    SIGN_UP_SUCCESS: null,
    REGISTER_ERROR: null,
    CLEAN_STATE_DATA: null,
  }
);