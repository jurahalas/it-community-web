import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as signUpActions from './SignUpActions';
import './SignUp.css';
import LoginizationWrapper from '../components/loginizationWrapper/LoginizationWrapper';
import SignUpForm from '../components/forms/signUp/SignUp';

class SignUp extends Component {
  signUp = (formData) => {
    this.props.signUpActions.signUpRequest(formData);
  };

  componentWillUnmount() {
    this.props.signUpActions.cleanStateData();
  }

  render() {
    const { signUpReducer, signUpActions } = this.props;

    return (
      <LoginizationWrapper>
        <SignUpForm
          signUpReducer={ signUpReducer }
          signUpActions={ signUpActions }
          onSubmit={ this.signUp }
        />
      </LoginizationWrapper>
    );
  }
}

SignUp.propTypes = {
  signUpReducer: PropTypes.object.isRequired,
  signUpActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    signUpReducer: state.signUpReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signUpActions: bindActionCreators(signUpActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);