import { signUpActionTypes } from './SignUpConstants';

const initialState = {
  formData: {
    name: '',
    email: '',
    password: '',
  },
  loading: false,
  errors: '',
};

export default function signUpReducer(state = initialState, action) {
  switch(action.type) {

    case signUpActionTypes.SIGN_UP_REQUEST:
      return {
        ...state,
        formData: action.payload.formData,
        loading: true,
      };

    case signUpActionTypes.SIGN_UP_SUCCESS:
      if(action.payload.token.hasOwnProperty('token')) {
        localStorage.access_token = action.payload.token.token;
        localStorage.email = action.payload.token.email;
        localStorage.name = action.payload.token.name;
      }

      return {
        ...state,
        errors: '',
        loading: false,
        token: action.payload.token.token,
      };

    case signUpActionTypes.REGISTER_ERROR:
      return {
        ...state,
        errors: action.payload.error,
        loading: false,
      };

    case signUpActionTypes.CLEAN_STATE_DATA:
      return {
        ...initialState,
      };

    default:
      return state;
  }
}