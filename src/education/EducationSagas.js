import { processRequest } from '../services/Api';
import { all, takeLatest } from 'redux-saga/effects';
import { put, call } from 'redux-saga/effects';
import { replace } from 'react-router-redux';
import { educationActionTypes } from './EducationConstants';
import * as notificationActions from '../components/notification/NotificationActions';
import * as educationActions from './EducationActions';
import * as profileActions from '../profile/ProfileActions';

export default function* () {
  yield all([
    yield takeLatest(educationActionTypes.CREATE_EDUCATION_REQUEST, handleCreateEducation),
    yield takeLatest(educationActionTypes.GET_EDUCATION_REQUEST, handleGetEducation),
    yield takeLatest(educationActionTypes.UPDATE_EDUCATION_REQUEST, handleUpdateEducation),
    yield takeLatest(educationActionTypes.DELETE_EDUCATION_REQUEST, handleDeleteEducation),
  ]);
}

export function* handleCreateEducation(action) {
  try {
    const { school, degree, fieldOfStudy, from, to, current, description } = action.payload.formData;

    const requestPayload = {
      school,
      degree,
      fieldOfStudy,
      from,
      to: current ? '' : to,
      current,
      description,
    };

    yield call(processRequest, 'profile/education', 'POST', requestPayload);
    yield put(educationActions.createEducationSuccess());
    yield put(replace('/dashboard'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during creating education!', true));
    yield put(educationActions.createEducationError(e));
  }
}

export function* handleGetEducation(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `profile/education/${ id }`);

    yield put(educationActions.getEducationSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during getting education!', true));
    yield put(educationActions.getEducationError(e));
  }
}

export function* handleUpdateEducation(action) {
  try {
    const { id, formData } = action.payload;
    const { school, degree, fieldOfStudy, from, to, current, description } = formData;

    const requestPayload = {
      school,
      degree,
      fieldOfStudy,
      from,
      to: current ? '' : to,
      current,
      description,
    };

    yield call(processRequest, `profile/education/${ id }`, 'POST', requestPayload);
    yield put(educationActions.updateEducationSuccess());
    yield put(replace('/dashboard'));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during updating education!', true));
    yield put(educationActions.createEducationError(e));
  }
}

export function* handleDeleteEducation(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `profile/education/${ id }`, 'DELETE');

    yield put(profileActions.getCurrentUserProfileSuccess(data));
    yield put(educationActions.deleteEducationSuccess());
  } catch(e) {
    yield put(notificationActions.createNotification('Error during deleting education', true));
    yield put(educationActions.deleteEducationError(e));
  }
}