import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import './Education.css';
import Notification from '../components/notification/Notification';
import CustomButton from '../components/customButton/CustomButton';
import Spinner from '../components/spinner/SpinnerGrid';
import { educationValidate } from '../services/FormValidate';
import InputComponent from '../components/inputComponent/InputComponent';
import { connect } from 'react-redux';
import * as educationActions from './EducationActions';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import Moment from 'moment/moment';

class Education extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: null,
    };
  }

  componentDidMount() {
    const { educationActions, route: { id } } = this.props;

    if(id)
      educationActions.getEducationRequest(id);
  }

  onCheck=() => {
    const { current } = this.state;

    if(current === null)
      this.setState({
        current: false,
      });

    else
      this.setState({
        current: !current,
      });
  };

  renderField = ({ disabled, label, type, mask, meta: { error, touched, invalid }, input: { onChange, value, onBlur, onFocus } }) => (
    <div className={ classNames('form-group', { 'has-error ': invalid && error && touched }) }>
      <InputComponent
        placeholder={ label }
        onChange={ onChange }
        value={ type === 'date' ? Moment(value).format('YYYY-MM-DD') : value }
        onBlur={ onBlur }
        onFocus={ onFocus }
        error={ error && touched }
        mask={ mask }
        type={ type }
        disabled={ disabled }
      />
      { invalid && error && touched
        ? (
          <div className='error-message-container'>
            <div className="error-message">{ error }</div>
          </div>
        )
        : (
          <div className="helpBlock" />
        )
      }
    </div>
  );

  onSubmit = (formData) => {
    const { educationActions, route: { id } } = this.props;

    if(id) {
      educationActions.updateEducationRequest({ ...formData, current: this.state.current }, id);
      this.props.initialize(true);
    }
    else {
      educationActions.createEducationRequest({ ...formData, current: this.state.current });
      this.props.initialize(true);
    }
  };

  openDashboardPage = () =>
    this.props.history.push('/dashboard');

  render() {
    const { educationReducer, handleSubmit, route: { id } } = this.props;
    const { current } = this.state;
    const checked = current || (current === null && educationReducer.current);

    return (
      <div className="education-container">
        <Spinner isShow={ educationReducer.loading } />
        <Notification />
        <div className="education-title-wrapper">
          <div className="education-title">
            Education
          </div>
          <div className='education-buttons-container'>
            <CustomButton
              text='Cancel'
              clickHandler={ this.openDashboardPage }
              className="cancel"
              fontFamily="Quicksand"
              height={ 35 }
            />
            <CustomButton
              text='Save'
              clickHandler={ handleSubmit(this.onSubmit) }
              className="submit"
              fontFamily="Quicksand"
              height={ 35 }
            />
          </div>
        </div>
        <div className="education-detail">
          <div className='header-form'>
            <h1>{ id ? 'Edit' : 'Add' } Education</h1>
            <h3>Add any school, bootcamp, etc...</h3>
            <p>*= required fields</p>
          </div>
          <form className='education-form' onSubmit={ handleSubmit(this.onSubmit) }>
            <div className='education-block-item'>
              <Field
                name="school"
                type="text"
                component={ this.renderField }
                label="* School"
              />
              <Field
                name="degree"
                type="text"
                component={ this.renderField }
                label="* Degree or Certification"
              />
              <Field
                name="fieldOfStudy"
                type="text"
                component={ this.renderField }
                label="* Field of Study"
              />
              <h4>* From Date</h4>
              <Field
                name="from"
                type="date"
                component={ this.renderField }
                label="Enter data from"
              />
              {
                !checked &&
                <div>
                  <h4>* To Date</h4>
                  <Field
                    name="to"
                    type="date"
                    component={ this.renderField }
                    label="Enter data from"
                    disabled={ checked }
                  />
                </div>
              }
              <div className="form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  name="current"
                  value={ checked }
                  checked={ checked }
                  onChange={ this.onCheck }
                  id="current"
                />
                <label htmlFor="current" className="form-check-label">
                  Current Job
                </label>
              </div>
              <Field
                name="description"
                type="text"
                component={ this.renderField }
                label="Program Description"
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Education = reduxForm({
  form: 'Education',
  validate: educationValidate,
  enableReinitialize: true,
})(Education);

Education.propTypes = {
  educationActions: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func,
  initialize: PropTypes.func,
  history: PropTypes.object.isRequired,
  educationReducer: PropTypes.object.isRequired,
  route: PropTypes.shape({
    id: PropTypes.string,
  }),
};

function mapStateToProps(state) {
  return {
    educationReducer: state.educationReducer,
    initialValues: state.educationReducer,
    form: state.formReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    educationActions: bindActionCreators(educationActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Education);