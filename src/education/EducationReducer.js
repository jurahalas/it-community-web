import { educationActionTypes } from './EducationConstants';

const initialState = {
  school: '',
  degree: '',
  fieldOfStudy: '',
  from: '',
  to: '',
  current: false,
  description: '',
  error: {
    error: '',
    status: '',
  },
  loading: false,
};

export default function educationReducer(state = initialState, action) {
  switch(action.type) {

    case educationActionTypes.GET_EDUCATION_REQUEST:
    case educationActionTypes.CREATE_EDUCATION_REQUEST:
    case educationActionTypes.UPDATE_EDUCATION_REQUEST:
    case educationActionTypes.DELETE_EDUCATION_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case educationActionTypes.GET_EDUCATION_SUCCESS:
      return {
        ...state,
        ...action.payload.data[0],
        loading: false,
      };

    case educationActionTypes.GET_EDUCATION_ERROR:
      return {
        ...state,
        error: {
          error: action.payload.errors,
          status: action.payload.errors.status,
        },
        loading: false,
      };

    case educationActionTypes.CREATE_EDUCATION_SUCCESS:
    case educationActionTypes.UPDATE_EDUCATION_SUCCESS:
    case educationActionTypes.DELETE_EDUCATION_SUCCESS:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}