import keyMirror from 'keymirror';

export const educationActionTypes = keyMirror(
  {
    GET_EDUCATION_REQUEST: null,
    GET_EDUCATION_SUCCESS: null,
    GET_EDUCATION_ERROR: null,
    CREATE_EDUCATION_REQUEST: null,
    CREATE_EDUCATION_SUCCESS: null,
    CREATE_EDUCATION_ERROR: null,
    UPDATE_EDUCATION_REQUEST: null,
    UPDATE_EDUCATION_SUCCESS: null,
    UPDATE_EDUCATION_ERROR: null,
    DELETE_EDUCATION_REQUEST: null,
    DELETE_EDUCATION_SUCCESS: null,
    DELETE_EDUCATION_ERROR: null,
  }
);