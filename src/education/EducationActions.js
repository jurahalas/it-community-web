import { educationActionTypes } from './EducationConstants';

export function getEducationRequest(id) {
  return {
    type: educationActionTypes.GET_EDUCATION_REQUEST,
    payload: {
      id,
    },
  };
}

export function getEducationSuccess(data) {
  return {
    type: educationActionTypes.GET_EDUCATION_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getEducationError(errors) {
  return {
    type: educationActionTypes.GET_EDUCATION_ERROR,
    payload: {
      errors,
    },
  };
}

export function createEducationRequest(formData) {
  return {
    type: educationActionTypes.CREATE_EDUCATION_REQUEST,
    payload: {
      formData,
    },
  };
}

export function createEducationSuccess(data) {
  return {
    type: educationActionTypes.CREATE_EDUCATION_SUCCESS,
    payload: {
      data,
    },
  };
}

export function createEducationError(errors) {
  return {
    type: educationActionTypes.CREATE_EDUCATION_ERROR,
    payload: {
      errors,
    },
  };
}

export function updateEducationRequest(formData, id) {
  return {
    type: educationActionTypes.UPDATE_EDUCATION_REQUEST,
    payload: {
      formData,
      id,
    },
  };
}

export function updateEducationSuccess(data) {
  return {
    type: educationActionTypes.UPDATE_EDUCATION_SUCCESS,
    payload: {
      data,
    },
  };
}

export function updateEducationError(errors) {
  return {
    type: educationActionTypes.UPDATE_EDUCATION_ERROR,
    payload: {
      errors,
    },
  };
}

export function deleteEducationRequest(id) {
  return {
    type: educationActionTypes.DELETE_EDUCATION_REQUEST,
    payload: {
      id,
    },
  };
}

export function deleteEducationSuccess() {
  return {
    type: educationActionTypes.DELETE_EDUCATION_SUCCESS,
  };
}

export function deleteEducationError(errors) {
  return {
    type: educationActionTypes.DELETE_EDUCATION_ERROR,
    payload: {
      errors,
    },
  };
}