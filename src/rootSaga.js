import { fork } from 'redux-saga/effects';
import signInSagas from './signIn/SignInSagas';
import signUpSagas from './signUp/SignUpSagas';
import userProfileSagas from './profile/ProfileSagas';
import experienceSagas from './experience/ExperienceSagas';
import educationSagas from './education/EducationSagas';
import postsSagas from './posts/PostsSagas';

export default function* rootSaga() {
  yield fork(signInSagas);
  yield fork(signUpSagas);
  yield fork(userProfileSagas);
  yield fork(experienceSagas);
  yield fork(educationSagas);
  yield fork(postsSagas);
}