import { processRequest } from '../services/Api';
import { all, takeLatest, put, call } from 'redux-saga/effects';
import { postsActionTypes } from './PostsConstants';
import * as notificationActions from '../components/notification/NotificationActions';
import * as postsActions from './PostsActions';

export default function* () {
  yield all([
    yield takeLatest(postsActionTypes.GET_CURRENT_USER_POSTS_REQUEST, handleGetCurrentUserPosts),
    yield takeLatest(postsActionTypes.GET_POSTS_REQUEST, handleGetPosts),
    yield takeLatest(postsActionTypes.CREATE_POSTS_REQUEST, handleCreatePost),
    yield takeLatest(postsActionTypes.ADD_LIKE_REQUEST, handleAddLike),
    yield takeLatest(postsActionTypes.ADD_DIS_LIKE_REQUEST, handleAddDisLike),
    yield takeLatest(postsActionTypes.DELETE_POST_REQUEST, handleDeletePost),
    yield takeLatest(postsActionTypes.GET_POST_DETAILS_REQUEST, handleGetPost),
    yield takeLatest(postsActionTypes.CREATE_COMMENT_REQUEST, handleCreateComment),
    yield takeLatest(postsActionTypes.DELETE_COMMENT_REQUEST, handleDeleteComment),
  ]);
}

export function* handleGetCurrentUserPosts() {
  try {
    const { data } = yield call(processRequest, 'posts/byCurrentUser');

    yield put(postsActions.getPostsSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get posts data!`, true));
    yield put(postsActions.getPostsError(e));
  }
}

export function* handleGetPosts() {
  try {
    const { data } = yield call(processRequest, 'posts');

    yield put(postsActions.getPostsSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get posts data!`, true));
    yield put(postsActions.getPostsError(e));
  }
}

export function* handleCreatePost(action) {
  try {
    const { post } = action.payload.data;
    const avatar = localStorage.avatar || localStorage.googleAvatar || '';
    const requestPayload = {
      text: post,
      name: localStorage.name,
      avatar,
    };

    const { data } = yield call(processRequest, 'posts', 'POST', requestPayload);

    yield put(postsActions.createPostsSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during creating post!', true));
    yield put(postsActions.createPostsError(e));
  }
}

export function* handleAddDisLike(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `posts/unlike/${id}`, 'POST');

    yield put(postsActions.addLikeSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during adding like!', true));
    yield put(postsActions.addLikeError(e));
  }
}

export function* handleAddLike(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `posts/like/${id}`, 'POST');

    yield put(postsActions.addLikeSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during adding like!', true));
    yield put(postsActions.addLikeError(e));
  }
}

export function* handleDeletePost(action) {
  try {
    const { id } = action.payload;

    const { data } = yield call(processRequest, `posts/${id}`, 'DELETE');

    yield put(postsActions.addLikeSuccess(data));
    yield put(notificationActions.createNotification('Post successfully deleted!', false));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during deleting post!', true));
    yield put(postsActions.addLikeError(e));
  }
}

export function* handleGetPost(action) {
  try {
    const { id } = action.payload;
    const { data } = yield call(processRequest, `posts/${ id }`);

    yield put(postsActions.getPostDetailsSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification(`Can't get post data!`, true));
    yield put(postsActions.getPostDetailsError(e));
  }
}

export function* handleCreateComment(action) {
  try {
    const { comment, id } = action.payload;
    const { post } = comment;
    const avatar = localStorage.avatar || localStorage.googleAvatar || '';
    const requestPayload = {
      text: post,
      name: localStorage.name,
      avatar,
    };

    const { data } = yield call(processRequest, `posts/comment/${ id }`, 'POST', requestPayload);

    yield put(postsActions.createCommentSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during creating post!', true));
    yield put(postsActions.createCommentError(e));
  }
}

export function* handleDeleteComment(action) {
  try {
    const { postId, commentId } = action.payload;

    const { data } = yield call(processRequest, `posts/comment/${postId}/${commentId}`, 'DELETE');

    yield put(postsActions.deleteCommentSuccess(data));
  } catch(e) {
    yield put(notificationActions.createNotification('Error during deleting comment!', true));
    yield put(postsActions.deleteCommentError(e));
  }
}
