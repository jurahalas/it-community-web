import keyMirror from 'keymirror';

export const postsActionTypes = keyMirror(
  {
    GET_CURRENT_USER_POSTS_REQUEST: null,
    GET_POSTS_REQUEST: null,
    GET_POSTS_SUCCESS: null,
    GET_POSTS_ERROR: null,
    CREATE_POSTS_REQUEST: null,
    CREATE_POSTS_SUCCESS: null,
    CREATE_POSTS_ERROR: null,
    ADD_LIKE_REQUEST: null,
    ADD_LIKE_SUCCESS: null,
    ADD_LIKE_ERROR: null,
    ADD_DIS_LIKE_REQUEST: null,
    ADD_DIS_LIKE_SUCCESS: null,
    ADD_DIS_LIKE_ERROR: null,
    DELETE_POST_REQUEST: null,
    DELETE_POST_SUCCESS: null,
    DELETE_POST_ERROR: null,
    GET_POST_DETAILS_REQUEST: null,
    GET_POST_DETAILS_SUCCESS: null,
    GET_POST_DETAILS_ERROR: null,
    CREATE_COMMENT_REQUEST: null,
    CREATE_COMMENT_SUCCESS: null,
    CREATE_COMMENT_ERROR: null,
    DELETE_COMMENT_REQUEST: null,
    DELETE_COMMENT_SUCCESS: null,
    DELETE_COMMENT_ERROR: null,
  }
);