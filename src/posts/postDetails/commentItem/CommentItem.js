import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import userAvatar from '../../../images/avatar.svg';
import './CommentItem.css';
import CusttomBottom from '../../../components/customButton/CustomButton';
import jwt_decode from 'jwt-decode';
import { bindActionCreators } from 'redux';
import * as postsActions from '../../PostsActions';

class CommentItem extends Component {
  onDeleteClick=() => {
    const { comment, postId, postsActions } = this.props;

    postsActions.deleteCommentRequest(comment._id, postId);
  };

  render() {
    const { comment } = this.props;
    const { avatar, name, text, user } = comment || {};
    const { id } = jwt_decode(localStorage.access_token);

    return (
      <div className="comment-item-container">
        <div className="comment-item-image-container">
          <img
            className="comment-item-image"
            src={ avatar || userAvatar }
            alt=""
          />
          <br />
          <h2>{ name }</h2>
        </div>
        <div className="comment-item-details-container">
          <h2>{ text }</h2>
          <div className='likes-buttons-container'>
            { user === id &&
            <CusttomBottom
              text='Delete'
              className='delete-btn'
              color='#ffffff'
              height={ 30 }
              clickHandler={ this.onDeleteClick }
            />
            }
          </div>
        </div>
      </div>
    );
  }
}

CommentItem.propTypes = {
  postsActions: PropTypes.func.isRequired,
  comment: PropTypes.object.isRequired,
  postId: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(CommentItem);