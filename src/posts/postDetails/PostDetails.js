import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PostItem from '../postItem/PostItem';
import Spinner from '../../components/spinner/SpinnerGrid';
import PostForm from '../../components/forms/post/PostForm';
import * as postsActions from '../PostsActions';
import { bindActionCreators } from 'redux';
import CommentItem from './commentItem/CommentItem';
import './PostDetails.css';
import io from 'socket.io-client';

class PostDetails extends Component {
  constructor(props) {
    super(props);

    this.socket = io('localhost:5001');

    const getCommentDetails = () => {
      this.props.postsActions.getPostDetailsRequest(this.props.match.params.id);
    };

    this.socket.on('getComment', () => {
      getCommentDetails();
    });
  }

  componentDidMount() {
    this.props.postsActions.getPostDetailsRequest(this.props.match.params.id);
  }

  createComment =(data) => {
    const { postsActions, match } = this.props;

    postsActions.createCommentRequest(data, match.params.id);
  };

  render() {
    const { post, loading } = this.props.postsReducer;
    const { _id, comments } = post || {};

    return (
      <div className="post-detail-container">
        <Spinner isShow={ loading } />
        {
          post ? (
            <Fragment>
              <PostItem post={ post } showActions={ false } />
              <PostForm
                onSubmit={ this.createComment }
              />
              { comments &&
              comments.map(comment => (
                <CommentItem key={ comment._id } comment={ comment } postId={ _id } />
              ))
              }
            </Fragment>
          )
            : (
              <h1>No content</h1>
            )
        }
      </div>
    );
  }
}

PostDetails.propTypes = {
  postsActions: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  postsReducer: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }),
};

function mapStateToProps(state) {
  return {
    postsReducer: state.postsReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);
