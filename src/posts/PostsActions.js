import { postsActionTypes } from './PostsConstants';

export function getCurrentUserPostsRequest() {
  return {
    type: postsActionTypes.GET_CURRENT_USER_POSTS_REQUEST,
  };
}

export function getPostsRequest() {
  return {
    type: postsActionTypes.GET_POSTS_REQUEST,
  };
}

export function getPostsSuccess(data) {
  return {
    type: postsActionTypes.GET_POSTS_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getPostsError(errors) {
  return {
    type: postsActionTypes.GET_POSTS_ERROR,
    payload: {
      errors,
    },
  };
}

export function createPostsRequest(data) {
  return {
    type: postsActionTypes.CREATE_POSTS_REQUEST,
    payload: {
      data,
    },
  };
}

export function createPostsSuccess(data) {
  return {
    type: postsActionTypes.CREATE_POSTS_SUCCESS,
    payload: {
      data,
    },
  };
}

export function createPostsError(errors) {
  return {
    type: postsActionTypes.CREATE_POSTS_ERROR,
    payload: {
      errors,
    },
  };
}

export function addLikeRequest(id) {
  return {
    type: postsActionTypes.ADD_LIKE_REQUEST,
    payload: {
      id,
    },
  };
}

export function addLikeSuccess(data) {
  return {
    type: postsActionTypes.ADD_LIKE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function addLikeError(errors) {
  return {
    type: postsActionTypes.ADD_LIKE_ERROR,
    payload: {
      errors,
    },
  };
}

export function addDisLikeRequest(id) {
  return {
    type: postsActionTypes.ADD_DIS_LIKE_REQUEST,
    payload: {
      id,
    },
  };
}

export function addDisLikeSuccess(data) {
  return {
    type: postsActionTypes.ADD_DIS_LIKE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function addDisLikeError(errors) {
  return {
    type: postsActionTypes.ADD_DIS_LIKE_ERROR,
    payload: {
      errors,
    },
  };
}

export function deletePostRequest(id) {
  return {
    type: postsActionTypes.DELETE_POST_REQUEST,
    payload: {
      id,
    },
  };
}

export function deletePostSuccess(data) {
  return {
    type: postsActionTypes.DELETE_POST_SUCCESS,
    payload: {
      data,
    },
  };
}

export function deletePostError(errors) {
  return {
    type: postsActionTypes.DELETE_POST_ERROR,
    payload: {
      errors,
    },
  };
}

export function getPostDetailsRequest(id) {
  return {
    type: postsActionTypes.GET_POST_DETAILS_REQUEST,
    payload: {
      id,
    },
  };
}

export function getPostDetailsSuccess(data) {
  return {
    type: postsActionTypes.GET_POST_DETAILS_SUCCESS,
    payload: {
      data,
    },
  };
}

export function getPostDetailsError(errors) {
  return {
    type: postsActionTypes.GET_POST_DETAILS_ERROR,
    payload: {
      errors,
    },
  };
}

export function createCommentRequest(comment, id) {
  return {
    type: postsActionTypes.CREATE_COMMENT_REQUEST,
    payload: {
      comment,
      id,
    },
  };
}

export function createCommentSuccess(data) {
  return {
    type: postsActionTypes.CREATE_COMMENT_SUCCESS,
    payload: {
      data,
    },
  };
}

export function createCommentError(errors) {
  return {
    type: postsActionTypes.CREATE_COMMENT_ERROR,
    payload: {
      errors,
    },
  };
}

export function deleteCommentRequest(commentId, postId) {
  return {
    type: postsActionTypes.DELETE_COMMENT_REQUEST,
    payload: {
      commentId,
      postId,
    },
  };
}

export function deleteCommentSuccess(data) {
  return {
    type: postsActionTypes.DELETE_COMMENT_SUCCESS,
    payload: {
      data,
    },
  };
}

export function deleteCommentError(errors) {
  return {
    type: postsActionTypes.DELETE_COMMENT_ERROR,
    payload: {
      errors,
    },
  };
}