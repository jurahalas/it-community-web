import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import userAvatar from '../../images/avatar.svg';
import './PostItem.css';
import CusttomBottom from '../../components/customButton/CustomButton';
import likeIcon from '../../images/like.svg';
import likeFillIcon from '../../images/like-fill.svg';
import disLikeIcon from '../../images/dislike.svg';
import disLikeFillIcon from '../../images/disLikeFill.svg';
import jwt_decode from 'jwt-decode';
import { bindActionCreators } from 'redux';
import * as postsActions from '../PostsActions';
import { withRouter } from 'react-router-dom';

class PostItem extends Component {
  onDeleteClick = () => {
    const { postsActions, post } = this.props;

    postsActions.deletePostRequest(post._id);
  };

  onLikeClick=() => {
    const { postsActions, post } = this.props;

    postsActions.addLikeRequest(post._id);
  };

  onDisLikeClick=() => {
    const { postsActions, post } = this.props;

    postsActions.addDisLikeRequest(post._id);
  };

  onCommentsClick=() => {
    const { history, post } = this.props;

    history.push(`/post/${post._id}`);
  };

  findUserLike = (likes, id) => (likes.filter(like => like.user === id).length > 0);

  render() {
    const { showActions, post } = this.props;
    const { avatar, name, text, likes, disLikes, user } = post || {};
    const { id } = jwt_decode(localStorage.access_token);

    return (
      <div className="post-item-container">
        <div className="post-item-image-container">
          <img
            className="post-item-image"
            src={ avatar || userAvatar }
            alt=""
          />
          <br />
          <h2>{ name }</h2>
        </div>
        <div className="post-item-details-container">
          <h2>{ text }</h2>
          { showActions &&
            <div className='likes-buttons-container'>
              <div className='likes'>
                <img
                  src={ this.findUserLike(likes, id) ? likeFillIcon : likeIcon }
                  className='like'
                  alt=''
                  onClick={ this.onLikeClick }
                />
                <span className="likes-counter">{ likes.length }</span>
              </div>
              <div className='likes'>
                <img
                  src={ this.findUserLike(disLikes, id) ? disLikeFillIcon : disLikeIcon }
                  className='like'
                  alt=''
                  onClick={ this.onDisLikeClick }
                />
                <span className="likes-counter">{ disLikes.length }</span>
              </div>
              <CusttomBottom
                text='Comments'
                className='comment-btn'
                color='#ffffff'
                height={ 30 }
                clickHandler={ this.onCommentsClick }
              />
              { user === id &&
              <CusttomBottom
                text='Delete'
                className='delete-btn'
                color='#ffffff'
                height={ 30 }
                clickHandler={ this.onDeleteClick }
              />
              }
            </div>
          }
        </div>
      </div>
    );
  }
}

PostItem.defaultProps = {
  showActions: true,
};

PostItem.propTypes = {
  postsActions: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  showActions: PropTypes.bool,
};

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(withRouter(PostItem));