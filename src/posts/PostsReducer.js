import { postsActionTypes } from './PostsConstants';

const initialState = {
  posts: null,
  post: null,
  loading: false,
};

export default function postsReducer(state = initialState, action) {
  switch(action.type) {

    case postsActionTypes.GET_POSTS_REQUEST:
    case postsActionTypes.GET_CURRENT_USER_POSTS_REQUEST:
    case postsActionTypes.CREATE_POSTS_REQUEST:
    case postsActionTypes.ADD_LIKE_REQUEST:
    case postsActionTypes.ADD_DIS_LIKE_REQUEST:
    case postsActionTypes.DELETE_POST_REQUEST:
    case postsActionTypes.GET_POST_DETAILS_REQUEST:
    case postsActionTypes.CREATE_COMMENT_REQUEST:
    case postsActionTypes.DELETE_COMMENT_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case postsActionTypes.GET_POSTS_SUCCESS:
    case postsActionTypes.CREATE_POSTS_SUCCESS:
    case postsActionTypes.ADD_LIKE_SUCCESS:
    case postsActionTypes.ADD_DIS_LIKE_SUCCESS:
    case postsActionTypes.DELETE_POST_SUCCESS:
      return {
        ...state,
        posts: action.payload.data,
        loading: false,
      };

    case postsActionTypes.GET_POSTS_ERROR:
    case postsActionTypes.CREATE_POSTS_ERROR:
    case postsActionTypes.ADD_LIKE_ERROR:
    case postsActionTypes.ADD_DIS_LIKE_ERROR:
    case postsActionTypes.DELETE_POST_ERROR:
    case postsActionTypes.DELETE_COMMENT_ERROR:
      return {
        ...state,
        error: {
          error: action.payload.errors,
        },
        loading: false,
      };

    case postsActionTypes.GET_POST_DETAILS_SUCCESS:
    case postsActionTypes.CREATE_COMMENT_SUCCESS:
    case postsActionTypes.DELETE_COMMENT_SUCCESS:

      return {
        ...state,
        post: action.payload.data,
        loading: false,
      };

    default:
      return state;
  }
}