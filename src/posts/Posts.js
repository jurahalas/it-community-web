import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PostForm from '../components/forms/post/PostForm';
import Spinner from '../components/spinner/SpinnerGrid';
import * as postsActions from './PostsActions';
import { bindActionCreators } from 'redux';
import PostItem from './postItem/PostItem';
import './Posts.css';
import io from 'socket.io-client';

class Posts extends Component {
  constructor(props) {
    super(props);

    const getAllPosts = () => {
      if(this.props.route.name === 'MyPosts')
        this.props.postsActions.getCurrentUserPostsRequest(props.match.params.id);
      else
        this.props.postsActions.getPostsRequest();
    };

    this.socket = io('localhost:5001');

    this.socket.on('getAllPosts', () => {
      getAllPosts();
    });
  }

  createPost =(data) => {
    this.props.postsActions.createPostsRequest(data);
  };

  render() {
    const { posts, loading } = this.props.postsReducer;

    return (
      <div className="posts-container">
        <Spinner isShow={ loading } />
        <PostForm
          onSubmit={ this.createPost }
        />
        {
          posts && posts instanceof Array
            ? (
              posts.map(post => <PostItem key={ post._id } post={ post } />)
            )
            : (
              posts && <PostItem key={ posts._id } post={ posts } />
            )
        }
      </div>
    );
  }
}

Posts.propTypes = {
  postsActions: PropTypes.func.isRequired,
  postsReducer: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.object,
  }),
  route: PropTypes.shape({
    name: PropTypes.object,
  }),
};

function mapStateToProps(state) {
  return {
    postsReducer: state.postsReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postsActions: bindActionCreators(postsActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Posts);
