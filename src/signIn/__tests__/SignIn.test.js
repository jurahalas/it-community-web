import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import SignIn from '../SignIn';
import { Provider } from 'react-redux';
import Store from '../../store/Store';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  shallow(
    <Provider store={ Store }>
      <SignIn
        handleSubmit={ jest.fn() }
        signInActions={ {} }
        signIn={ {} }
        appReducer={ {} }
        errorPageActions={ {} }
        history={ { push: jest.fn() } }
        location={ {} }
        signUpActions={ {} }
      />
    </Provider>);
});