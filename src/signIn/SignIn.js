import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SignIn.css';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import LoginizationWrapper from '../components/loginizationWrapper/LoginizationWrapper';
import SignInForm from '../components/forms/signIn/SignIn';
import * as signInActions from './SignInActions';

class SignIn extends Component {
  login = (email, password) => {
    this.props.signInActions.loginRequest(email, password);
  };

  render() {
    return (
      <LoginizationWrapper>
        <SignInForm
          signIn={ this.props.signIn }
          onSubmit={ this.login }
        />
      </LoginizationWrapper>
    );
  }
}

SignIn.propTypes = {
  signIn: PropTypes.object.isRequired,
  signInActions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...state,
    signIn: state.signInReducer,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    signInActions: bindActionCreators(signInActions, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
