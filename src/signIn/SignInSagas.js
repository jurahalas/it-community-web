import { processRequest } from '../services/Api';
import { put, call, takeLatest, all } from 'redux-saga/effects';
import { SignInActionTypes } from './SignInConstants';
import * as signInActions from './SignInActions';
import { replace } from 'react-router-redux';
import * as notificationActions from '../components/notification/NotificationActions';

export default function* () {
  yield all([
    yield takeLatest(SignInActionTypes.LOGIN_REQUEST, handleLoginRequest),
  ]);
}

export function* handleLoginRequest(action) {
  try {
    const { email, password } = action.payload;
    const requestPayload = {
      email,
      password,
    };
    const token = yield call(processRequest, 'users/login', 'POST', requestPayload);

    if(token.data.user.hasOwnProperty('token')) {
      yield put(signInActions.loginSuccess(token.data.user));
      yield put(replace('/'));
    } else
      yield put(notificationActions.createNotification('Username or Password is incorrect!', true));
  } catch(e) {
    let message = 'Error during sign in!';

    if(e.response && e.response.data && e.response.data.errors)
      message = e.response.data.errors;

    yield put(notificationActions.createNotification(message, true));
    yield put(signInActions.loginError(e));
  }
}