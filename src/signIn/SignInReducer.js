import { SignInActionTypes } from './SignInConstants';

const initialState = {
  loading: false,
  error: '',
  token: {},
};

export default function signInReducer(state = initialState, action) {
  const { type, payload } = action;
  const { token } = payload || {};

  switch(type) {

    case SignInActionTypes.LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };

    case SignInActionTypes.LOGIN_SUCCESS:
      if(action.payload.token.hasOwnProperty('token')) {
        localStorage.access_token = token.token;
        localStorage.email = token.email;
        localStorage.name = token.email;
        localStorage.avatar = token.avatar || '';
        localStorage.googleAvatar = token.googleAvatar || '';
      }

      return {
        ...state,
        error: '',
        loading: false,
        token: action.payload.token.token,
      };

    case SignInActionTypes.LOGOUT:
      localStorage.removeItem('access_token');
      localStorage.removeItem('email');
      localStorage.removeItem('handle');
      localStorage.removeItem('avatar');
      localStorage.removeItem('googleAvatar');
      localStorage.removeItem('name');

      return {
        loading: false,
        error: '',
        token: {},
      };

    case SignInActionTypes.LOGIN_ERROR:
      return {
        ...state,
        error: action.payload.error.message,
        loading: false,
      };

    case SignInActionTypes.CHANGE_TOKEN:
      localStorage.access_token = action.payload.token;

      return state;

    default:
      return state;
  }
}