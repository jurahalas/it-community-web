import { SignInActionTypes } from './SignInConstants';

export function loginRequest(email, password) {
  return {
    type: SignInActionTypes.LOGIN_REQUEST,
    payload: {
      email,
      password,
    },
  };
}

export function loginSuccess(token) {
  return {
    type: SignInActionTypes.LOGIN_SUCCESS,
    payload: {
      token,
    },
  };
}

export function logout() {
  return {
    type: SignInActionTypes.LOGOUT,
  };
}

export function loginError(error) {
  return {
    type: SignInActionTypes.LOGIN_ERROR,
    error: true,
    payload: {
      error,
    },
  };
}

export function changeToken(token) {
  return {
    type: SignInActionTypes.CHANGE_TOKEN,
    payload: {
      token,
    },
  };
}