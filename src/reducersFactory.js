import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import notificationReducer from './components/notification/NotificationReducer';
import signInReducer from './signIn/SignInReducer';
import profileReducer from '../src/profile/ProfileReducer';
import signUpReducer from './signUp/SignUpReducer';
import experienceReducer from './experience/ExperienceReducer';
import educationReducer from './education/EducationReducer';
import postsReducer from './posts/PostsReducer';

export default combineReducers({
  form: formReducer,
  notificationReducer,
  signInReducer,
  signUpReducer,
  profileReducer,
  experienceReducer,
  educationReducer,
  postsReducer,
});
